import os
import torch
import numpy as np
import pandas as pd

from utils.test_utils import stable_cumsum
from utils.score_func import iterate_data_msp, iterate_data_odin, iterate_data_energy, iterate_data_mahalanobis, iterate_data_gradnorm, get_ood_scores
from utils.monitor import iterate_data_monitor
from utils.falsification import get_verdicts

from sklearn.linear_model import LogisticRegressionCV
import sklearn.metrics as sk
from torch.autograd import Variable

def get_accuracy(model, in_loader, logger, device):

    model.eval()

    with torch.no_grad():
        correct = 0
        total = 0
        for images, labels in in_loader:
            images = images.to(device)
            labels = labels.to(device)
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
        logger.info('Accuracy of the network on the in-distribution test images: {} %'.format(100 * correct / total))

def fpr_and_fdr_at_recall(y_true, y_score, recall_level, pos_label=1.):
    # make y_true a boolean vector
    y_true = (y_true == pos_label)

    # sort scores and corresponding truth values
    desc_score_indices = np.argsort(y_score, kind="mergesort")[::-1]
    y_score = y_score[desc_score_indices]
    y_true = y_true[desc_score_indices]

    # y_score typically has many tied values. Here we extract
    # the indices associated with the distinct values. We also
    # concatenate a value for the end of the curve.
    distinct_value_indices = np.where(np.diff(y_score))[0]
    threshold_idxs = np.r_[distinct_value_indices, y_true.size - 1]

    # accumulate the true positives with decreasing threshold
    tps = stable_cumsum(y_true)[threshold_idxs]
    fps = 1 + threshold_idxs - tps      # add one because of zero-based indexing

    thresholds = y_score[threshold_idxs]

    recall = tps / tps[-1]

    last_ind = tps.searchsorted(tps[-1])
    sl = slice(last_ind, None, -1)      # [last_ind::-1]
    recall, fps, tps, thresholds = np.r_[recall[sl], 1], np.r_[fps[sl], 0], np.r_[tps[sl], 0], thresholds[sl]

    cutoff = np.argmin(np.abs(recall - recall_level))

    return fps[cutoff] / (np.sum(np.logical_not(y_true))), thresholds[cutoff]   # , fps[cutoff]/(fps[cutoff] + tps[cutoff])


def get_measures(in_scores, out_scores, monitor_status='off', in_verdicts=None, out_verdicts=None):

    if monitor_status == 'on':
        verdicts = np.concatenate((in_verdicts, out_verdicts))
        idx_v0 = np.where(verdicts==0)[0] # accept
        idx_v1 = np.where(verdicts==1)[0] # reject
        idx_v2 = np.where(verdicts==2)[0] # uncertain

    num_in = len(in_scores)
    num_out = len(out_scores)
    recall_level = 0.95

    # ID as positive
    labels = np.zeros(num_in + num_out, dtype=np.int32)
    labels[:num_in] += 1

    scores = np.concatenate((in_scores, out_scores))
    if monitor_status == 'on':
        scores_range = np.max(scores) - np.min(scores)
        scores[idx_v0] += (scores_range + 1)
        scores[idx_v1] -= (scores_range + 1)

    aupr_in = sk.average_precision_score(labels, scores)
    auroc_in = sk.roc_auc_score(labels, scores)
    fpr_in, thr_in = fpr_and_fdr_at_recall(labels, scores, recall_level)

    # OoD as positive
    labels_rev = np.zeros(num_in + num_out, dtype=np.int32)
    labels_rev[num_in:] += 1
    scores_rev = -scores
    if monitor_status == 'on':
        scores_rev[idx_v0] -= (scores_range + 1)
        scores_rev[idx_v1] += (scores_range + 1)

    aupr_out = sk.average_precision_score(labels_rev, scores_rev)
    auroc_out = sk.roc_auc_score(labels_rev, scores_rev)
    fpr_out, thr_out = fpr_and_fdr_at_recall(labels_rev, scores_rev, recall_level)

    return auroc_in, auroc_out, aupr_in, aupr_out, fpr_in, fpr_out, thr_in, thr_out


def run_eval(model, in_loader, out_loader, logger, args, num_classes, device):
    # switch to evaluate mode
    model.eval()

    logger.info("Running test...")
    logger.flush()

    in_verdicts, out_verdicts = None, None
    if args.monitor.status == 'on':
        in_verdicts = iterate_data_monitor(in_loader, model, device, args.benchmark, args.monitor.layer, 
                                           args.monitor.storage_folder, range(num_classes), [args.monitor.taus], 
                                           pos=args.eval.pos, enriched=args.monitor.enrich)
        for i in range(args.eval.num_to_avg):
            out_verdicts = iterate_data_monitor(out_loader, model, device, args.benchmark, args.monitor.layer, 
                                                args.monitor.storage_folder, range(num_classes), [args.monitor.taus], 
                                                pos=args.eval.pos, n=args.eval.num_ood, seed=args.seed+i,
                                                enriched=args.monitor.enrich)
            
    for score in args.scores:

        auroc_in_list, aupr_in_list, fpr95_in_list, thr_in_list = [], [], [], []
        auroc_out_list, aupr_out_list, fpr95_out_list, thr_out_list = [], [], [], []
    
        # Processing in-distribution data
        if score == 'msp':
            in_scores = iterate_data_msp(in_loader, model, device, temper=args.scores.msp.T)
        elif score == 'odin':
            in_scores = iterate_data_odin(in_loader, model, device, epsilon=args.scores.odin.epsilon, 
                                          temper=args.scores.odin.T)
        elif score == 'energy':
            in_scores = iterate_data_energy(in_loader, model, device, temper=args.scores.energy.T)
        elif score == 'Mahalanobis':
            sample_mean, precision, lr_weights, lr_bias, magnitude = np.load(
                os.path.join(args.mahalanobis_param_path, 'results.npy'), allow_pickle=True)
            sample_mean = [s.to(device) for s in sample_mean]
            precision = [p.to(device) for p in precision]

            regressor = LogisticRegressionCV(cv=2).fit([[0, 0, 0, 0], [0, 0, 0, 0], [1, 1, 1, 1], [1, 1, 1, 1]],
                                                        [0, 0, 1, 1])

            regressor.coef_ = lr_weights
            regressor.intercept_ = lr_bias

            temp_x = torch.rand(2, 3, 480, 480)
            temp_x = Variable(temp_x).to(device)
            temp_list = model(x=temp_x, layer_index='all')[1]
            num_output = len(temp_list)

            in_scores = iterate_data_mahalanobis(in_loader, model, num_classes, sample_mean, precision,
                                                    num_output, magnitude, regressor, device)
        elif score == 'gradnorm':
            in_scores = iterate_data_gradnorm(in_loader, model, args.model.name, device, 
                                              temper=args.scores.gradnorm.T)
                
        # Processing out-of-distribution data
        for i in range(args.eval.num_to_avg):
            if score == 'msp':
                out_scores = iterate_data_msp(out_loader, model, device, temper=args.scores.msp.T, 
                                              n=args.eval.num_ood, seed=args.seed+i)
            elif score == 'odin':
                out_scores = iterate_data_odin(out_loader, model, device, epsilon=args.scores.odin.epsilon, 
                                               temper=args.scores.odin.T, n=args.eval.num_ood, seed=args.seed+i)
            elif score == 'energy':
                out_scores = iterate_data_energy(out_loader, model, device, temper=args.scores.energy.T, 
                                                 n=args.eval.num_ood, seed=args.seed+i)
            elif score == 'Mahalanobis':
                out_scores = iterate_data_mahalanobis(out_loader, model, num_classes, sample_mean, precision,
                                                        num_output, magnitude, regressor, device, n=args.eval.num_ood, 
                                                        seed=args.seed+i)
            elif score == 'gradnorm':
                out_scores = iterate_data_gradnorm(out_loader, model, args.model.name, device, 
                                                   temper=args.scores.gradnorm.T, n=args.eval.num_ood, seed=args.seed+i)
            else:
                raise ValueError("Unknown score type {}".format(args.score))

            auroc_in, auroc_out, aupr_in, aupr_out, fpr95_in, fpr95_out, thr_in, thr_out = get_measures(in_scores, out_scores, 
                                                                                                        args.monitor.status, 
                                                                                                        in_verdicts=in_verdicts, 
                                                                                                        out_verdicts=out_verdicts)
            
            auroc_in_list.append(auroc_in); aupr_in_list.append(aupr_in); fpr95_in_list.append(fpr95_in); thr_in_list.append(thr_in)
            auroc_out_list.append(auroc_out); aupr_out_list.append(aupr_out); fpr95_out_list.append(fpr95_out); thr_out_list.append(thr_out)
        
        auroc_in_mean = np.mean(auroc_in_list); aupr_in_mean = np.mean(aupr_in_list)
        fpr95_in_mean = np.mean(fpr95_in_list); thr_in_mean = np.mean(thr_in_list)
        auroc_out_mean = np.mean(auroc_out_list); aupr_out_mean = np.mean(aupr_out_list)
        fpr95_out_mean = np.mean(fpr95_out_list); thr_out_mean = np.mean(thr_out_list)

        logger.info('------------Results for {}------------'.format(score))
        logger.info('AUROC(In): {}'.format(auroc_in_mean * 100))
        logger.info('AUPR (In): {}'.format(aupr_in_mean * 100))
        logger.info('FPR95(In): {}'.format(fpr95_in_mean * 100))
        logger.info('Threshold(In): {}'.format(thr_in_mean))
        logger.info('AUROC(Out): {}'.format(auroc_out_mean * 100))
        logger.info('AUPR (Out): {}'.format(aupr_out_mean * 100))
        logger.info('FPR95(Out): {}'.format(fpr95_out_mean * 100))
        logger.info('Threshold(Out): {}'.format(thr_out_mean))
        
        logger.flush()

def test_prediction_verdicts(model, dataset, dataloader, temp_set_name, device, args): 
    pred_labels = torch.empty(0, dtype=torch.uint8).to(device)
    true_labels_0 = torch.empty(0, dtype=torch.uint8)
    model.eval()
    for data in dataloader:
        X, y = data[0].to(device), data[1]
        with torch.no_grad():
            output = model(X)
        y_pred = torch.argmax(output, axis=-1)
        pred_labels = torch.concat((pred_labels, y_pred))
        true_labels_0 = torch.concat((true_labels_0, y))
    
    img_idx = [sp_path.split('/')[-1].removesuffix('.png') for sp_path, _ in dataset.samples]
    pred_labels = pred_labels.cpu()
    true_labels = torch.zeros(pred_labels.shape, dtype=torch.uint8)

    if args.sampling.temp_distribution == 'ood': # ood dataset
        true_labels[:] = torch.max(pred_labels) + 1
    else:
        for k, v in dataset.class_to_idx.items():
            true_labels[true_labels_0 == v] = int(k)
    
    # Use threshold@TPR=95, calculate OoD scores & verdicts of different OoD detectors using score functions 
    ood_scores = get_ood_scores(model, dataloader, device, args)
    verdicts = get_verdicts(args, ood_scores)

    # Use tau@TPR=95, calculate verdicts of abstraction-based monitors
    num_classes = 10 if args.benchmark == "CIFAR10" else 100
    monitor_verdict = iterate_data_monitor(dataloader, model, device, args.benchmark, args.monitor.layer, 
                                           args.monitor.storage_folder, range(num_classes), args.monitor.taus, 
                                           pos=args.eval.pos)
    
    if args.monitor.enrich: 
        refined_monitor_verdict = iterate_data_monitor(dataloader, model, device, args.benchmark, 
                                                       args.monitor.layer, args.monitor.storage_folder, 
                                                       range(num_classes), args.monitor.refined_taus, 
                                                       pos=args.eval.pos, enriched=True)  # TODO
    
    # Record test results in file
    folder_path = './checkpoints/intermediate_results/'+args.benchmark+'/test_dataset/'
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)
    file_name = temp_set_name+'.csv'
    data = {
            'idx': img_idx,
            'pred_labels': pred_labels,
            'true_labels': true_labels,
           }
    df = pd.DataFrame(data)
    for score_function, verdict in verdicts.items():
        df[score_function] = verdict
    df['monitor_layer_'+args.monitor.layer] = monitor_verdict
    if args.monitor.enrich:
        df['refined_monitor_layer_'+args.monitor.layer] = refined_monitor_verdict
    df.to_csv(folder_path+file_name, index=False)

def robust_eval(temp_set_name, logger, args):

    folder_path = './checkpoints/intermediate_results/'+args.benchmark+'/test_dataset/'
    
    record = dict()
    for suffix in ['temp', 'gen_amp_'+str(args.sampling.method.a)]:
        record[suffix] = dict()
        file_name = temp_set_name+'_'+suffix+'.csv'
        target = 1 if args.eval.pos == args.sampling.temp_distribution else 0

        df = pd.read_csv(folder_path+file_name)
        idx = df['idx'].values.astype(str)
        pred_labels = df['pred_labels'].values
        true_labels = df['true_labels'].values
        model_true_idx = np.where(pred_labels == true_labels)[0]

        verdicts = dict()
        verdicts_true_idx = dict()
        for score_function in args.scores.keys():
            verdicts[score_function] = df[score_function].values
            verdicts_true_idx[score_function] = np.where(verdicts[score_function] == target)[0]
        verdicts['monitor_layer_'+args.monitor.layer] = df['monitor_layer_'+args.monitor.layer].values
        verdicts_true_idx['monitor_layer_'+args.monitor.layer] = np.where(verdicts['monitor_layer_'+args.monitor.layer] == target)[0]
        if args.monitor.enrich:
            verdicts['refined_monitor_layer_'+args.monitor.layer] = df['refined_monitor_layer_'+args.monitor.layer].values
            verdicts_true_idx['refined_monitor_layer_'+args.monitor.layer] = np.where(verdicts['refined_monitor_layer_'+args.monitor.layer] == target)[0]

        logger.info(f"============ Dataset name: {temp_set_name+'_'+suffix} =============")
        logger.info(f"Size of dataset: {len(true_labels)}.")
        logger.info(f"Number correctly predicted by CNN model: {len(model_true_idx)}.")
        for score_function in args.scores.keys():
            logger.info(f"Number correctly verdicted by {score_function}: {len(verdicts_true_idx[score_function])}.")
        logger.info(f"Number correctly verdicted by monitor_layer_{args.monitor.layer}: {len(verdicts_true_idx['monitor_layer_'+args.monitor.layer])}.")
        if args.monitor.enrich:
            logger.info(f"Number correctly verdicted by refined_monitor_layer_{args.monitor.layer}: {len(verdicts_true_idx['refined_monitor_layer_'+args.monitor.layer])}.")

        record[suffix]['idx'] = idx
        record[suffix]['pred_labels'] = pred_labels
        record[suffix]['true_labels'] = true_labels
        record[suffix]['verdicts'] = verdicts
        record[suffix]['verdicts_true_idx'] = verdicts_true_idx

    # Robust test
    idx_origin = [id.split('_')[0] for id in record['gen_amp_'+str(args.sampling.method.a)]['idx']]
    idx_origin_index = [int(np.where(record['temp']['idx'] == id)[0]) for id in idx_origin]
    pred_labels_origin = record['temp']['pred_labels'][idx_origin_index]
    true_labels_origin = record['temp']['true_labels'][idx_origin_index]

    model_true2false_idx = np.where(np.logical_and(pred_labels_origin == true_labels_origin, record['gen_amp_'+str(args.sampling.method.a)]['pred_labels'] != true_labels_origin))[0]
    model_false2true_idx = np.where(np.logical_and(pred_labels_origin != true_labels_origin, record['gen_amp_'+str(args.sampling.method.a)]['pred_labels'] == true_labels_origin))[0]
    model_false2false_idx = np.where(np.logical_and(np.logical_and(pred_labels_origin != true_labels_origin, 
                                                                   record['gen_amp_'+str(args.sampling.method.a)]['pred_labels'] != true_labels_origin),
                                                                   pred_labels_origin != record['gen_amp_'+str(args.sampling.method.a)]['pred_labels']))[0]

    model_robust_idx = np.where(record['gen_amp_'+str(args.sampling.method.a)]['pred_labels'] == pred_labels_origin)[0]

    verdicts_origin = dict()
    ood_detector_true2false_idx = dict()
    ood_detector_false2true_idx = dict()
    ood_detector_robust_idx = dict()
    for score_function in args.scores.keys(): 
        verdicts_origin[score_function] = record['temp']['verdicts'][score_function][idx_origin_index]
        ood_detector_true2false_idx[score_function] = np.where(np.logical_and(verdicts_origin[score_function] == target, record['gen_amp_'+str(args.sampling.method.a)]['verdicts'][score_function] != target))[0]
        ood_detector_false2true_idx[score_function] = np.where(np.logical_and(verdicts_origin[score_function] != target, record['gen_amp_'+str(args.sampling.method.a)]['verdicts'][score_function] == target))[0]
        ood_detector_robust_idx[score_function] = np.where(record['gen_amp_'+str(args.sampling.method.a)]['verdicts'][score_function] == verdicts_origin[score_function])[0]
    verdicts_origin['monitor_layer_'+args.monitor.layer] = record['temp']['verdicts']['monitor_layer_'+args.monitor.layer][idx_origin_index]
    ood_detector_true2false_idx['monitor_layer_'+args.monitor.layer] = np.where(np.logical_and(verdicts_origin['monitor_layer_'+args.monitor.layer] == target, 
                                                                                               record['gen_amp_'+str(args.sampling.method.a)]['verdicts']['monitor_layer_'+args.monitor.layer] != target))[0]
    ood_detector_false2true_idx['monitor_layer_'+args.monitor.layer] = np.where(np.logical_and(verdicts_origin['monitor_layer_'+args.monitor.layer] != target, 
                                                                                               record['gen_amp_'+str(args.sampling.method.a)]['verdicts']['monitor_layer_'+args.monitor.layer] == target))[0]
    ood_detector_robust_idx['monitor_layer_'+args.monitor.layer] = np.where(record['gen_amp_'+str(args.sampling.method.a)]['verdicts']['monitor_layer_'+args.monitor.layer] == verdicts_origin['monitor_layer_'+args.monitor.layer])[0]
    if args.monitor.enrich:
        verdicts_origin['refined_monitor_layer_'+args.monitor.layer] = record['temp']['verdicts']['refined_monitor_layer_'+args.monitor.layer][idx_origin_index]
        ood_detector_true2false_idx['refined_monitor_layer_'+args.monitor.layer] = np.where(np.logical_and(verdicts_origin['refined_monitor_layer_'+args.monitor.layer] == target, 
                                                                                                           record['gen_amp_'+str(args.sampling.method.a)]['verdicts']['refined_monitor_layer_'+args.monitor.layer] != target))[0]
        ood_detector_false2true_idx['refined_monitor_layer_'+args.monitor.layer] = np.where(np.logical_and(verdicts_origin['refined_monitor_layer_'+args.monitor.layer] != target, 
                                                                                                           record['gen_amp_'+str(args.sampling.method.a)]['verdicts']['refined_monitor_layer_'+args.monitor.layer] == target))[0]
        ood_detector_robust_idx['refined_monitor_layer_'+args.monitor.layer] = np.where(record['gen_amp_'+str(args.sampling.method.a)]['verdicts']['refined_monitor_layer_'+args.monitor.layer] == verdicts_origin['refined_monitor_layer_'+args.monitor.layer])[0]

    logger.info(f"============ Robustness Test =============")
    logger.info(f"Number of true->false model predictions: {len(model_true2false_idx)}.")
    logger.info(f"Number of false->true model predictions: {len(model_false2true_idx)}.")
    logger.info(f"Number of false->false model predictions: {len(model_false2false_idx)}.")
    logger.info(f"Number of robust model predictions: {len(model_robust_idx)}.")
    for score_function in args.scores.keys():
        logger.info(f"Number of true->false ood detections by {score_function}: {len(ood_detector_true2false_idx[score_function])}.")
        logger.info(f"Number of false->true ood detections by {score_function}: {len(ood_detector_false2true_idx[score_function])}.")
        logger.info(f"Number of robust ood detections by {score_function}: {len(ood_detector_robust_idx[score_function])}.")
    logger.info(f"Number of true->false ood detections by monitor_layer_{args.monitor.layer}: {len(ood_detector_true2false_idx['monitor_layer_'+args.monitor.layer])}.")
    logger.info(f"Number of false->true ood detections by monitor_layer_{args.monitor.layer}: {len(ood_detector_false2true_idx['monitor_layer_'+args.monitor.layer])}.")
    logger.info(f"Number of robust ood detections by monitor_layer_{args.monitor.layer}: {len(ood_detector_robust_idx['monitor_layer_'+args.monitor.layer])}.")
    if args.monitor.enrich:
        logger.info(f"Number of true->false ood detections by refined_monitor_layer_{args.monitor.layer}: {len(ood_detector_true2false_idx['refined_monitor_layer_'+args.monitor.layer])}.")
        logger.info(f"Number of false->true ood detections by refined_monitor_layer_{args.monitor.layer}: {len(ood_detector_false2true_idx['refined_monitor_layer_'+args.monitor.layer])}.")
        logger.info(f"Number of robust ood detections by refined_monitor_layer_{args.monitor.layer}: {len(ood_detector_robust_idx['refined_monitor_layer_'+args.monitor.layer])}.")



