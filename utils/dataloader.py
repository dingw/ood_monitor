import torch
import torchvision as tv
from torch.utils.data import ConcatDataset
from utils.test_utils import setup_seed

def load_dataset_from_folder(data_dir, dataset_name, args, split=None, shuffle=False, raw=False):
    
    if raw:
        test_tx = tv.transforms.Compose([
        tv.transforms.Resize((args.preprocess.input_size, args.preprocess.input_size), antialias=True),
        tv.transforms.PILToTensor(),
        ])
    else: 
        if dataset_name in ['LSUN-C', 'LSUN-R', 'iSUN']:
            test_tx = tv.transforms.Compose([
                tv.transforms.ToTensor(),
                tv.transforms.Normalize(tuple(args.preprocess.mean), tuple(args.preprocess.std)),
            ])
        else:
            test_tx = tv.transforms.Compose([
                    tv.transforms.Resize((args.preprocess.input_size, args.preprocess.input_size), antialias=True),
                    tv.transforms.ToTensor(),
                    tv.transforms.Normalize(tuple(args.preprocess.mean), tuple(args.preprocess.std)),
                ])
    if split is not None:
        dataset = tv.datasets.ImageFolder(data_dir+dataset_name.replace('-','_')+"/"+split+"/", test_tx)
    else:
        dataset = tv.datasets.ImageFolder(data_dir+dataset_name.replace('-','_')+"/", test_tx)
    dataloader = torch.utils.data.DataLoader(dataset, batch_size=args.preprocess.batch_size, shuffle=shuffle,
                                             num_workers=args.preprocess.workers, pin_memory=True, drop_last=False)
    
    return dataset, dataloader

def load_dataset_from_torchvision(save_dir, dataset_name, split, args, shuffle=False, raw=False, download=True):
    
    if raw:
        test_tx = tv.transforms.Compose([
        tv.transforms.Resize((args.preprocess.input_size, args.preprocess.input_size), antialias=True),
        tv.transforms.PILToTensor(),
        ])

    else: 
        test_tx = tv.transforms.Compose([
        tv.transforms.Resize((args.preprocess.input_size, args.preprocess.input_size), antialias=True),
        tv.transforms.ToTensor(),
        tv.transforms.Normalize(tuple(args.preprocess.mean), tuple(args.preprocess.std)),
        ])

    if dataset_name == "CIFAR10":
        dataset = tv.datasets.CIFAR10(save_dir+dataset_name, train=True if split=="train" else False,
                                      transform=test_tx,
                                      download=download)
    elif dataset_name == "CIFAR100":
        dataset = tv.datasets.CIFAR100(save_dir+dataset_name, train=True if split=="train" else False,
                                       transform=test_tx,
                                       download=download)
    elif dataset_name == "Textures":
        dataset1 = tv.datasets.DTD(save_dir+dataset_name, split='train',
                                  transform=test_tx,
                                  download=download)
        dataset2 = tv.datasets.DTD(save_dir+dataset_name, split='test',
                                  transform=test_tx,
                                  download=download)
        dataset3 = tv.datasets.DTD(save_dir+dataset_name, split='val',
                                  transform=test_tx,
                                  download=download)
        dataset = ConcatDataset([dataset1, dataset2, dataset3])


    elif dataset_name == "SVHN":
        dataset = tv.datasets.SVHN(save_dir+dataset_name, split=split,
                                   transform=test_tx,
                                   download=download)
    elif dataset_name == "Places365":
        dataset = tv.datasets.Places365(save_dir+dataset_name, split="val",
                                        small=True,
                                        transform=test_tx,
                                        download=download)
    elif dataset_name == "Food101":
        dataset = tv.datasets.Food101(save_dir+dataset_name, split=split,
                                      transform=test_tx,
                                      download=download)
    elif dataset_name == "Flowers102":
        dataset = tv.datasets.Flowers102(save_dir+dataset_name, split=split,
                                         transform=test_tx,
                                         download=download)
    elif dataset_name == "GTSRB":
        dataset = tv.datasets.GTSRB(save_dir+dataset_name, split=split,
                                         transform=test_tx,
                                         download=download)
    # elif dataset_name == "FER2013":
    #     dataset = tv.datasets.FER2013(save_dir+dataset_name, split=split,
    #                                   transform=test_tx,
    #                                  )
    elif dataset_name == "EuroSAT":
        dataset = tv.datasets.EuroSAT(save_dir+dataset_name,
                                      transform=test_tx,
                                      download=download)
    elif dataset_name == "FakeData": # Gaussian Noise
        dataset = tv.datasets.FakeData(size=args.eval.num_ood,
                                       image_size=(3, args.preprocess.input_size, args.preprocess.input_size),
                                       num_classes=1,
                                        transform=test_tx,
                                        random_offset=args.seed)
    
    dataloader = torch.utils.data.DataLoader(dataset, batch_size=args.preprocess.batch_size, 
                                                shuffle=shuffle, num_workers=args.preprocess.workers, 
                                                pin_memory=True, drop_last=False)
    
    return dataset, dataloader


def make_id_ood(args, logger=None, raw=False, download=False):
    
    """Returns train and validation datasets."""
    # n_channels = 3 # TODO
    
    # in_set, in_loader = load_dataset_from_folder(args.data.id.folder, args.data.id.name, args, 
    #                                              split="test", shuffle=False, raw=raw)
    in_set, in_loader = load_dataset_from_torchvision(args.data.id.folder, args.data.id.name, "test", 
                                                args, shuffle=False, raw=raw, download=download)
    
    if logger is not None:
        logger.info(f"Using an in-distribution set with {len(in_set)} images.") 
        logger.info(f"Using an out-of-distribution set with {args.eval.num_ood} images.")

    out_set_list, out_loader_list = [], []
    shuffle_flag = args.eval.num_ood is not None
    for out_data_name in args.data.ood.name:
        if out_data_name in ["LSUN-C", "LSUN-R", "iSUN", "Imagenet-C", "Imagenet-R"]:
            out_set, out_loader = load_dataset_from_folder(args.data.ood.folder, out_data_name, 
                                                           args, shuffle=shuffle_flag, raw=raw)
        else:
            out_set, out_loader = load_dataset_from_torchvision(args.data.ood.folder, out_data_name, "test", 
                                                          args, shuffle=shuffle_flag, raw=raw, download=download)

        out_set_list.append(out_set); out_loader_list.append(out_loader)

    return in_set, out_set_list, in_loader, out_loader_list
