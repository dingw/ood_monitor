import torch
import numpy as np
from torch.autograd import Variable
from utils.mahalanobis_lib import get_Mahalanobis_score

from utils.test_utils import setup_seed

def iterate_data_msp(data_loader, model, device, temper=1.0, n=None, seed=None):
    confs = []
    smax = torch.nn.Softmax(dim=-1).to(device)
    if seed is not None:
        setup_seed(seed)
    for b, data in enumerate(data_loader):
        x, y = data[0], data[1]

        with torch.no_grad():
            x = x.to(device)
            logits = model(x)

            conf, _ = torch.max(smax(logits / temper), dim=-1)
            confs.extend(conf.data.cpu().numpy())
            if n is not None and len(confs) >= n:
                confs = confs[:n]
                break

    return np.array(confs)


def iterate_data_odin(data_loader, model, device, epsilon=0.0, temper=1000, n=None, seed=None):
    criterion = torch.nn.CrossEntropyLoss().to(device)
    confs = []
    if seed is not None:
        setup_seed(seed)
    for b, data in enumerate(data_loader):
        with torch.enable_grad():
            x, y = data[0], data[1]
            x = Variable(x.to(device), requires_grad=True)
            model.zero_grad()
            outputs = model(x)

            maxIndexTemp = np.argmax(outputs.data.cpu().numpy(), axis=1)
            outputs = outputs / temper

            labels = Variable(torch.LongTensor(maxIndexTemp).to(device))
            loss = criterion(outputs, labels)
            loss.backward()

            # Normalizing the gradient to binary in {0, 1}
            gradient = torch.ge(x.grad.data, 0)
            gradient = (gradient.float() - 0.5) * 2

            # TODO
            gradient[:,0] = (gradient[:,0] )/(63.0/255.0)
            gradient[:,1] = (gradient[:,1] )/(62.1/255.0)
            gradient[:,2] = (gradient[:,2] )/(66.7/255.0)

            tempInputs = torch.add(x.data, gradient, alpha=-epsilon)
            outputs = model(Variable(tempInputs))
            outputs = outputs / temper
            # Calculating the confidence after adding perturbations
            nnOutputs = outputs.data.cpu().numpy()
            nnOutputs = nnOutputs - np.max(nnOutputs, axis=1, keepdims=True)
            nnOutputs = np.exp(nnOutputs) / np.sum(np.exp(nnOutputs), axis=1, keepdims=True)

            confs.extend(np.max(nnOutputs, axis=1))
            if n is not None and len(confs) >= n:
                confs = confs[:n]
                break

    return np.array(confs)


def iterate_data_energy(data_loader, model, device, temper=1.0, n=None, seed=None):
    confs = []
    if seed is not None:
        setup_seed(seed)
    for b, data in enumerate(data_loader):
        with torch.no_grad():
            x, y = data[0], data[1]
            x = x.to(device)
            logits = model(x)

            conf = temper * torch.logsumexp(logits / temper, dim=1)
            confs.extend(conf.data.cpu().numpy())
            if n is not None and len(confs) >= n:
                confs = confs[:n]
                break

    return np.array(confs)


def iterate_data_mahalanobis(data_loader, model, num_classes, sample_mean, precision,
                             num_output, magnitude, regressor, device, n=None, seed=None):
    confs = []
    if seed is not None:
        setup_seed(seed)
    for b, data in enumerate(data_loader):
        with torch.no_grad():
            x, y = data[0], data[1]
            x = x.to(device)

            Mahalanobis_scores = get_Mahalanobis_score(x, model, num_classes, sample_mean, precision, num_output, magnitude)
            scores = -regressor.predict_proba(Mahalanobis_scores)[:, 1]
            confs.extend(scores)
            if n is not None and len(confs) >= n:
                confs = confs[:n]
                break
        
    return np.array(confs)


def iterate_data_gradnorm(data_loader, model, model_name, device, temper=1.0, n=None, seed=None):
    confs = []
    logsoftmax = torch.nn.LogSoftmax(dim=-1).to(device)
    if seed is not None:
        setup_seed(seed)
    for b, data in enumerate(data_loader):
        with torch.enable_grad():
            x, y = data[0], data[1]
            inputs = Variable(x.to(device), requires_grad=True)

            for data_i in inputs:
                model.zero_grad()
                output = model(data_i.unsqueeze(0))
                targets = torch.ones((1, output.shape[-1])).to(device)
                output = output / temper
                loss = torch.mean(-targets * logsoftmax(output), dim=-1)
                loss.backward()
            
                if model_name == 'resnetv2':
                    layer_grad = model.head.conv.weight.grad.data
                elif model_name == 'wrn':
                    layer_grad = model.fc.weight.grad.data

                layer_grad_norm = torch.sum(torch.abs(layer_grad)).cpu().numpy()

                confs.append(layer_grad_norm)
                if n is not None and len(confs) >= n:
                    confs = confs[:n]
                    return np.array(confs)
        
    return np.array(confs)


def get_ood_scores(model, test_loader, device, args, n=None, seed=None):
    ood_scores = dict()
    model.eval()
    for score in args.scores:
        # Processing in-distribution data
        if score == 'msp':
            scores = iterate_data_msp(test_loader, model, device, temper=args.scores.msp.T, n=n, 
                                      seed=seed)
        elif score == 'odin':
            scores = iterate_data_odin(test_loader, model, device, epsilon=args.scores.odin.epsilon, 
                                       temper=args.scores.odin.T, n=n, seed=seed)
        elif score == 'energy':
            scores = iterate_data_energy(test_loader, model, device, temper=args.scores.energy.T, n=n,
                                         seed=seed)
        elif score == 'gradnorm':
            scores = iterate_data_gradnorm(test_loader, model, args.model.name, device, 
                                              temper=args.scores.gradnorm.T, n=n, seed=seed)
        ood_scores[score] = scores
        
    return ood_scores