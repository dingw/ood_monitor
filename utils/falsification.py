import os
import torch
import torchvision as tv
import random
from sklearn.metrics import pairwise_distances
from utils.test_utils import setup_seed
from PIL import Image
import time
import numpy as np
from utils.score_func import get_ood_scores
from utils.dataloader import load_dataset_from_folder, load_dataset_from_torchvision
from utils.monitor import iterate_data_monitor


def get_verdicts(args, ood_scores):

    verdicts = dict()
    for score_func, scores in ood_scores.items():
        # Processing in-distribution data
        if score_func == 'msp':
            theta = args.scores.msp.theta
        elif score_func == 'odin':
            theta = args.scores.odin.theta
        elif score_func == 'energy':
            theta =args.scores.energy.theta
        elif score_func == 'gradnorm':
            theta = args.scores.gradnorm.theta
        
        verdicts_in = (scores > theta) * 1  # id as positive 
        verdicts_out = ((-scores) > theta) * 1 # ood as positive

        if args.eval.pos == 'id':
            verdicts[score_func] = verdicts_in
        elif args.eval.pos == 'ood':
            verdicts[score_func] = verdicts_out

    return verdicts

# Generate noisy ID/OOD dataset via random sampling
def gen_noisy_data(test_loader_raw, logger, args):

    # to save generated dataset
    folder_path = args.sampling.save_path + '/' + args.sampling.temp_name.replace('-', '_') + \
                  '_gen_amp_'+str(args.sampling.method.a)
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)
    folder_path_ori = args.sampling.save_path + '/' + args.sampling.temp_name.replace('-', '_') + '_temp'
    if not os.path.exists(folder_path_ori):
        os.makedirs(folder_path_ori)

    # Obtain template samples
    X_temp = torch.empty(0, dtype=torch.float32)
    y_temp = torch.empty(0, dtype=torch.uint8)

    setup_seed(args.seed)

    for data in test_loader_raw:
        X = data[0]; y = data[1]
        X_temp = torch.concat((X_temp, X))
        y_temp = torch.concat((y_temp, y))
        if args.sampling.n_temp != "all":
            if len(y_temp) >= args.sampling.n_temp:
                break

    if args.sampling.n_temp != "all":
        X_temp = X_temp[:args.sampling.n_temp]
        y_temp = y_temp[:args.sampling.n_temp]
    
    # Generate noisy samples from template samples
    start_time = time.time()
    gen_data = torch.empty(0, dtype=torch.float32)
    gen_labels = torch.empty(0, dtype=torch.uint8)
    idx_origin = []

    n_gen = args.sampling.n_gen

    for idx, data in enumerate(zip(X_temp, y_temp)):
        X_0 = data[0]; y_0 = data[1]
        y_0 = y_0.item()

        # save original image
        label_folder = os.path.join(folder_path_ori, str(y_0))
        if not os.path.exists(label_folder):
            os.makedirs(label_folder)
        data_image = Image.fromarray(X_0.permute(1, 2, 0).numpy().astype('uint8'))
        image_filename = os.path.join(label_folder, f'{str(idx)}.png')
        data_image.save(image_filename)

        # folder to save generated images
        label_folder = os.path.join(folder_path, str(y_0))
        if not os.path.exists(label_folder):
            os.makedirs(label_folder)

        X_temp = X_0.clone().view(-1).int()
        X_gen = torch.vstack([X_temp]*n_gen)
        
        if args.sampling.method.name == 'uniform':
            a = args.sampling.method.a
            noise = torch.randint(-a, a, X_gen.shape)
            X_gen += noise
            X_gen = torch.clamp(X_gen, 0, 255) # Clip the tensor between 0 and 255

        # TODO
        elif args.sampling.method.name == 'pixel':
            num_px = args.sampling.method.n_px
            # change 2 pixels
            px_id = random.sample(range(0, len(X_gen)), num_px)
            px_val = torch.randint(0, 256, (num_px,))
            X_gen[px_id] = px_val
                
        elif args.sampling.method.name == 'gaussian':
            # Define the mean and covariance matrix for the Gaussian distribution
            std = args.sampling.method.std
            X_gen += (torch.randn(X_gen.shape) * std).int()
            X_gen = torch.clamp(X_gen, 0, 255)  # Clip the tensor between 0 and 255
        
        # reshape & normalize generated data
        X_gen = X_gen.view(n_gen, X_0.shape[0], X_0.shape[1], X_0.shape[2])
        gen_data = torch.concat((gen_data, X_gen))
        gen_labels = torch.concat((gen_labels, torch.ones(n_gen, dtype=torch.int8)*y_0))
        idx_origin.append(idx)

        # save generated image
        for i, X_gen_i in enumerate(X_gen):
            data_image = Image.fromarray(X_gen_i.permute(1, 2, 0).numpy().astype('uint8'))
            image_filename = os.path.join(label_folder, f'{str(idx)}_{str(i+1)}.png')
            data_image.save(image_filename)

    last_time = time.time() - start_time 
    logger.info(f"{len(gen_data)} noisy samples generated from {args.sampling.n_temp} template samples")
    logger.info(f"Generated noisy dataset. Time taken: {last_time}")

    return gen_data, gen_labels
