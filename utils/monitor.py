import os
import pickle
import random

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import torch
import torchvision as tv
from sklearn.cluster import KMeans
from sklearn.metrics import pairwise_distances

from utils.test_utils import setup_seed
from utils.dataset import CustomDataset
from utils.dataloader import make_id_ood


# collect prediction and produced features at monitored layer for a model and dataset
def prediction_and_features(model, data_loader, device, moni_layers=[], n=None, seed=None):
    
    # set model to evaluate mode
    model.eval()
    
    y_true_all = torch.tensor([], dtype=torch.int8, device=device)
    y_pred_all = torch.tensor([], dtype=torch.int8, device=device)
    outputs = torch.tensor([], device=device)
    moni_features_all = {moni_layer: torch.tensor([], device=device) for moni_layer in moni_layers}
    
    if seed is not None:
        setup_seed(seed)
    for data in data_loader:
        x = data[0].to(device)
        y = data[1].to(device)
        with torch.no_grad():   

            layer_list, out_list = model.forward_feature_list(x)
            
            outputs = out_list[-1]
            _, y_pred = torch.max(outputs, 1)

            y_true_all = torch.cat((y_true_all, y), 0)
            y_pred_all = torch.cat((y_pred_all, y_pred), 0)
            for moni_layer in moni_layers:
                moni_features = out_list[int(moni_layer.replace('minus_', '-'))]
                moni_features_all[moni_layer] = torch.cat((moni_features_all[moni_layer], moni_features), 0)
            
            if n is not None and len(y_true_all) >= n:
                y_true_all = y_true_all[:n]
                y_pred_all = y_pred_all[:n]
                for moni_layer in moni_layers:
                    moni_features_all[moni_layer] = moni_features_all[moni_layer][:n]
                break
    
    y_true_all = y_true_all.cpu().numpy()  
    y_pred_all = y_pred_all.cpu().numpy()
    for moni_layer, moni_features in moni_features_all.items():
        moni_features_all[moni_layer] = moni_features.cpu().numpy()
    
    return y_true_all, y_pred_all, moni_features_all

def sort_class_wise_features(y_true, y_pred, moni_features_dict, monitor_storage_folder): # TODO
    """
    arguments:three numpy array of size (n, 1), (n, 1), (n, number_of_monitored_neurons)
    - y_true: ground truth of a dataset
    - y_pred: model output class predictions of a dataset
    - moni_features: neuron activation values at a monitored layer
    - monitor_storage_folder: the path for the folder where the results are saved
    
    return:
    let num_classes be the number of output classes
    - num_classes csv files: each contains the correctly classified monitored features as label i
    """
    for moni_layer, moni_features in moni_features_dict.items():
        if len(y_true) == len(y_pred) and len(y_pred)==moni_features.shape[0]:  # check the consistency of inputs dimensionality
            # creat index for each sample
            index_values = [i for i in range(len(y_true))]
            
            # name each monitored neuron
            num_moni_neurons = moni_features.shape[1]
            neuron_names = ["n"+str(i) for i in range(1, num_moni_neurons+1)]
            
            # creating the dataframe
            df = pd.DataFrame(data=moni_features, columns=neuron_names)
            
            # insert prediction, ground_truth, and index in turn
            df.insert(0, "pred_label", y_pred)
            df.insert(0, "true_label", y_true)
            df.insert(0, "index", index_values)

            # classify the features by its true and predicted labels
            labels_set = set(y_pred)
            for y in labels_set:
                # select good features correctly classified as class y
                features_correct = df.loc[(df["true_label"]==y) & (df["pred_label"]==y)]  
                
                # select bad features incorrectly classified as class y
                features_incorrect = df.loc[(df["true_label"]!=y) & (df["pred_label"]==y)]    
                
                # save features into a csv file
                folder_path = monitor_storage_folder + "/moni_features/Layer_" + moni_layer + "/"
                # check if the targeted directory exists, if not, creat that one
                if not os.path.exists(folder_path):
                    os.makedirs(folder_path)
                feature_file_path = folder_path + "class_" + str(y)
                features_correct.to_csv(feature_file_path + "_correctly_classified_features.csv", index = False)
                features_incorrect.to_csv(feature_file_path + "_incorrectly_classified_features.csv", index = False)
        else:
            return print("The dimensionalities of inputs are not consistent.")

# values: a two-dimensional array, m number of n-dimensional vectors to be clustered;
def modified_kmeans_cluster(values_to_cluster, threshold=None, k_start=None, n_clusters=None):
    n_values = len(values_to_cluster)
    print("n_samples =", n_values)
    if n_clusters is not None:      
        n_clusters = min(n_clusters, n_values)
        print("clustering with n_clusters =", n_clusters)
        kmeans = KMeans(n_clusters=n_clusters, random_state=0).fit(values_to_cluster)
    else:
        print("clustering with tau =", threshold)
        n_clusters = k_start
        assert n_values > 0
        kmeans = KMeans(n_clusters=n_clusters, random_state=0).fit(values_to_cluster)
        inertias = [kmeans.inertia_]
        while n_values > n_clusters:
            n_clusters_new = n_clusters + 1
            kmeans_new = KMeans(n_clusters=n_clusters_new, random_state=0).fit(values_to_cluster)
            inertias.append(kmeans_new.inertia_)
            if terminate_clustering(inertias, threshold):
                break
            kmeans = kmeans_new
            n_clusters += 1
    print("Result n_clusters =", max(kmeans.labels_)+1)
    return kmeans.labels_


def terminate_clustering(inertias, threshold):
    # method: compute relative improvement toward previous step
    assert len(inertias) > 1
    if inertias[-2] == 0:
        improvement = 0
    else:
        improvement = 1 - (inertias[-1] / inertias[-2])
    return improvement < threshold
       
def cluster_existed_features(classes, layer, taus, monitor_storage_folder, enriched=False):
    if len(taus) == len(classes):
        taus = [round(tau, ndigits=5) if tau > 0.00001 else tau for tau in taus]
    elif len(taus) == 1:
        taus = [round(taus[0], ndigits=5) if taus[0] > 0.00001 else taus[0]] * len(classes)
    
    appendixes = ["_correctly_classified_features.csv", "_incorrectly_classified_features.csv"]

    n_samples = []
    
    for i, y in enumerate(classes):
        for appendix in appendixes:
            # load data for class y at layer minus i
            enriched_appendix = "_enriched" if enriched else ""
            features_file_path = monitor_storage_folder + "/moni_features" + enriched_appendix + "/Layer_" + \
                                layer + "/class_" + str(y) + appendix
            df = pd.read_csv(features_file_path, dtype={"index": str})
            # index_values = df["index"].to_numpy()
            values_to_cluster = df[df.columns[3:]].to_numpy()
            n_samples.append(len(values_to_cluster))

            if len(values_to_cluster):
                # specify path and then load existing clustering results
                k_and_taus = dict()
                taus_existed = []
                clustering_results = pd.DataFrame(df, columns=["index", "true_label", "pred_label"])
                clustering_results_path = monitor_storage_folder + "/moni_features" + enriched_appendix + "/Layer_" + \
                                          layer + "/clustering_results_class_" + str(y) + appendix

                if os.path.exists(clustering_results_path):
                    clustering_results = pd.read_csv(clustering_results_path,  dtype={"index": str})
                    for col in clustering_results.columns[3:]:
                        k_and_taus[col] = clustering_results[col].max() + 1

                # update the existing values of tau
                taus_existed = [float(key) for key in k_and_taus.keys()]

                if taus[i] not in taus_existed:
                    tau = taus[i]
                    if tau <= 1:
                        k_start = 1
                        bigger_taus = [x for x in taus_existed if x > tau and x <= 1]
                        if len(bigger_taus):
                            tau_closest = min(bigger_taus) 
                            k_start = k_and_taus[str(tau_closest)]
                        cluster_labels = modified_kmeans_cluster(values_to_cluster, threshold=tau, k_start=k_start)
                    else: # if tau > 1, we cluster by number
                        cluster_labels = modified_kmeans_cluster(values_to_cluster, n_clusters=round(tau))

                    clustering_results[str(tau)] = cluster_labels
                    taus_existed.append(tau)
                    k_and_taus[str(tau)] = max(cluster_labels) + 1

                clustering_results.to_csv(clustering_results_path, index=False)

    return n_samples

class Box:
    def __init__(self):
        self.dimensions = None
        self.ivals = []
        self.element_indexes = [] # record this box is built for what samples
        self.low_bound_indexes = dict() # record which samples visit the low bound for each dimension
        self.high_bound_indexes = dict() # record which samples visit the low bound for each dimension

    def build(self, dimensions, points):
        # a point is a tuple (index, n-dim numpy)
        # index = point[0]
        # value = point[1]
        piter = iter(points)
        self.dimensions = dimensions
        self.ivals = []
        self.element_indexes = []
        self.low_bound_indexes = dict()
        self.high_bound_indexes = dict()

        try:
            point = next(piter)
        except StopIteration:
            return
        else:
            self.element_indexes.append(point[0]) # update index list
            i = 0
            for coord in point[1]:
                if(i >= self.dimensions):
                    break
                self.ivals.append([coord, coord])
                self.low_bound_indexes["n"+str(i+1)] = [point[0]] # update low bound visiting index list
                self.high_bound_indexes["n"+str(i+1)] = [point[0]] # update upper bound visiting index list
                i += 1
            if(len(self.ivals) != self.dimensions):
                raise "IllegalArgument"

        while True:
            try:
                point = next(piter)
            except StopIteration:
                break
            else:
                self.element_indexes.append(point[0]) # update index list
                i = 0
                for coord in point[1]:
                    if(i >= self.dimensions):
                        break
                    ival = self.ivals[i]
                    if(coord < ival[0]):
                        ival[0] = coord
                        self.low_bound_indexes["n"+str(i+1)] = [point[0]] # update the bound and its index
                    elif(coord == ival[0]):
                        low_index_list = self.low_bound_indexes["n"+str(i+1)]
                        low_index_list.append(point[0])

                    if(coord > ival[1]):
                        ival[1] = coord
                        self.high_bound_indexes["n"+str(i+1)] = [point[0]] # update the bound and its index
                    elif(coord == ival[1]):
                        high_index_list = self.high_bound_indexes["n"+str(i+1)]
                        high_index_list.append(point[0])
                    i += 1

    def query(self, point):
        i = 0
        for coord in point[1]:
            if(i >= self.dimensions):
                break
            ival = self.ivals[i]
            if(coord < ival[0] or coord > ival[1]):
                return False
            i += 1
        return True

    def __str__(self):
        return self.ivals.__str__()


def boxes_query(point, boxes):
    for box in boxes:
        if len(box.ivals):
            if box.query(point):
                return True
            

class Monitor(object):

    def __init__(self, abs_type, netName, class_y, layer_i, good_ref=None, bad_ref=None):
        self.abs_type = abs_type
        self.netName = netName
        self.classification = class_y
        self.location = layer_i
        self.good_ref = good_ref
        self.bad_ref = bad_ref

    def set_reference(self, good_ref, bad_ref):
        self.good_ref = good_ref
        self.bad_ref = bad_ref
    
    def get_identity(self):
        print("Monitor for network:" + self.netName + "class: " + str(self.classification) + "at layer " + str(self.location))

    def make_verdicts(self, features):
        in_good_ref = []
        in_bad_ref = []
        if len(self.good_ref) and len(self.bad_ref):
            in_good_ref = ref_query(self.abs_type, features, self.good_ref)
            in_bad_ref = ref_query(self.abs_type, features, self.bad_ref)
            
        elif (not len(self.good_ref)) and len(self.bad_ref):
            in_good_ref = [False for x in features]
            in_bad_ref = ref_query(self.abs_type, features, self.bad_ref)
        
        elif len(self.good_ref) and (not len(self.bad_ref)):
            in_good_ref = ref_query(self.abs_type, features, self.good_ref)
            in_bad_ref = [False for x in features]

        else:
            in_good_ref = [False for x in features]
            in_bad_ref = [False for x in features]

        verdicts = query_infusion(in_good_ref, in_bad_ref)
        return verdicts

def ref_query(abs_type, features, reference):
    abstraction_types = {"Box": boxes_query} # {"Ball": balls_query, "Box": boxes_query,  "LCBox2": LCBox2es_query, "LCBox1": LCBox1es_query}
    if abs_type in abstraction_types:
        query_results = [abstraction_types[abs_type](x, reference) for x in features]
        return query_results
    else:
        print("Undefined type of abstraction")


def query_infusion(in_good_ref, in_bad_ref):
    if len(in_good_ref) == len(in_bad_ref): #0: acceptance (true, false), 1: rejection (false, true or false), 2: uncertainty (true, true)
        verdicts = np.zeros(len(in_good_ref), dtype=np.uint8)
        for i in range(len(in_good_ref)):
            if not in_good_ref[i]:
                verdicts[i] = 1
            elif in_bad_ref[i]:
                verdicts[i] = 2
        return verdicts
    else:
        print("Error: IllegalArgument")       


def monitors_offline_construction(network_name, classes, layer, taus, monitor_storage_folder, enriched=False):
    if len(taus) == len(classes):
        taus = [round(tau, ndigits=5) if tau > 0.00001 else tau for tau in taus]
    elif len(taus) == 1:
        taus = [round(taus[0], ndigits=5) if tau > 0.00001 else tau[0]] * len(classes)
    folder_type = "_enriched" if enriched else ""
    monitor_stored_folder_path = monitor_storage_folder + "/Monitors"+folder_type+"/"
    
    if not os.path.exists(monitor_stored_folder_path):
        os.makedirs(monitor_stored_folder_path)

    appendixes = ["_correctly_classified_features.csv", "_incorrectly_classified_features.csv"]

    for tau_i, y in enumerate(classes):
        
        # load obtained features to creat reference
        path_bad_features = monitor_storage_folder +"/moni_features"+folder_type+"/Layer_" + layer + "/class_" + str(y) + appendixes[1]
        path_good_features = monitor_storage_folder +"/moni_features"+folder_type+"/Layer_" + layer + "/class_" + str(y) + appendixes[0]
      
        bad_feat_clustering_results = []
        good_feat_clustering_results = []

        if os.path.exists(path_bad_features):
            df_bad_features = pd.read_csv(path_bad_features, dtype={"index": str})
            bad_features_to_cluster = df_bad_features[df_bad_features.columns[3:]].to_numpy()
            bad_features_index = df_bad_features["index"].to_numpy()
            
            # load clustering results to partition the features
            bad_feat_clustering_results_path = monitor_storage_folder + "/moni_features"+folder_type+"/Layer_" + layer + \
                "/clustering_results_class_" + str(y) + appendixes[1]
            
            if os.path.exists(bad_feat_clustering_results_path):
                bad_feat_clustering_results = pd.read_csv(bad_feat_clustering_results_path,  dtype={"index": str})
        else:
            print("Bad features path " + path_bad_features + " does not exist!")

        if os.path.exists(path_good_features):
            df_good_features = pd.read_csv(path_good_features,  dtype={"index": str})
            good_features_to_cluster = df_good_features[df_good_features.columns[3:]].to_numpy()
            good_features_index = df_good_features["index"].to_numpy()
            
            # load clustering results to partition the features
            good_feat_clustering_results_path = monitor_storage_folder + "/moni_features"+folder_type+"/Layer_" + layer + \
                "/clustering_results_class_" + str(y) + appendixes[0]
            n_dim = good_features_to_cluster.shape[1]
            
            if os.path.exists(good_feat_clustering_results_path):
                good_feat_clustering_results = pd.read_csv(good_feat_clustering_results_path,  dtype={"index": str})
        else:
            print("Good features path " + path_good_features + " does not exist!")
            
        tau = taus[tau_i]
        good_loc_boxes = []
        bad_loc_boxes = []

        monitor_stored_path = monitor_stored_folder_path + "monitor_for_class_" \
                                + str(y) + "_at_layer_" + layer + "_tau_" + str(tau) + ".pkl"
        if os.path.exists(monitor_stored_path):
            continue

        if len(bad_feat_clustering_results):
            # load clustering result related to tau
            bad_feat_clustering_result = bad_feat_clustering_results[str(tau)]
            # determine the labels of clusters
            bad_num_clusters = np.amax(bad_feat_clustering_result) + 1
            bad_clustering_labels = np.arange(bad_num_clusters)
            
            # extract the indices of vectors in a cluster
            bad_clusters_indices = []
            for k in bad_clustering_labels:
                bad_indices_cluster_k, = np.where(bad_feat_clustering_result == k)
                bad_clusters_indices.append(bad_indices_cluster_k)
            
            # creat local box for each cluster
            bad_loc_boxes = [Box() for _ in bad_clustering_labels]
            for j in range(len(bad_loc_boxes)):
                bad_points_j = [(bad_features_index[i], bad_features_to_cluster[i]) for i in bad_clusters_indices[j]]
                bad_loc_boxes[j].build(n_dim, bad_points_j)
            

        if len(good_feat_clustering_results):
            # load clustering result related to tau
            good_feat_clustering_result = good_feat_clustering_results[str(tau)]
            # determine the labels of clusters 
            good_num_clusters = np.amax(good_feat_clustering_result) + 1
            good_clustering_labels = np.arange(good_num_clusters)
            
            # extract the indices of vectors in a cluster
            good_clusters_indices = []
            for k in good_clustering_labels:
                good_indices_cluster_k, = np.where(good_feat_clustering_result == k)
                good_clusters_indices.append(good_indices_cluster_k)    
            
            # creat local box for each cluster
            good_loc_boxes = [Box() for _ in good_clustering_labels]
            for j in range(len(good_loc_boxes)):
                good_points_j = [(good_features_index[i], good_features_to_cluster[i]) for i in good_clusters_indices[j]]
                good_loc_boxes[j].build(n_dim, good_points_j)

        # creat the monitor for class y at layer i
        monitor_y_i = Monitor("Box", network_name, y, layer, good_ref=good_loc_boxes, bad_ref=bad_loc_boxes)
        # save the created monitor
        
        with open(monitor_stored_path, 'wb') as f:
            pickle.dump(monitor_y_i, f)


# collect prediction and produced features at monitored layer for a model and dataset
def prediction_and_features(model, data_loader, device, moni_layers=[], n=None, seed=None):
    
    # set model to evaluate mode
    model.eval()
    
    y_true_all = torch.tensor([], dtype=torch.int8, device=device)
    y_pred_all = torch.tensor([], dtype=torch.int8, device=device)
    outputs = torch.tensor([], device=device)
    moni_features_all = {moni_layer: torch.tensor([], device=device) for moni_layer in moni_layers}
    
    # deactivate autograd engine and reduce memory usage and speed up computations
    if seed is not None:
        setup_seed(seed)
    for data in data_loader:
        x = data[0].to(device)
        y = data[1].to(device)
        with torch.no_grad():      

            layer_list, out_list = model.forward_feature_list(x)
            
            outputs = out_list[-1]
            _, y_pred = torch.max(outputs, 1)

            y_true_all = torch.cat((y_true_all, y), 0)
            y_pred_all = torch.cat((y_pred_all, y_pred), 0)

            for moni_layer in moni_layers:
                moni_features = out_list[int(moni_layer.replace('minus_', '-'))]
                moni_features_all[moni_layer] = torch.cat((moni_features_all[moni_layer], moni_features), 0)
            
            if n is not None and len(y_true_all) >= n:
                y_true_all = y_true_all[:n]
                y_pred_all = y_pred_all[:n]
                for moni_layer in moni_layers:
                    moni_features_all[moni_layer] = moni_features_all[moni_layer][:n]
                break
    
    y_true_all = y_true_all.cpu().numpy()  
    y_pred_all = y_pred_all.cpu().numpy()
    for moni_layer, moni_features in moni_features_all.items():
        moni_features_all[moni_layer] = moni_features.cpu().numpy()
    
    return y_true_all, y_pred_all, moni_features_all


# test the monitors' verdicts for a network at specified layers and clustering parameters
def iterate_data_monitor(data_loader, model, device, benchmark, moni_layer, monitor_storage_folder, classes,
                         taus, n=None, seed=None, pos='ood', enriched=False):
    
    if len(taus) == 1:
        taus = taus * len(classes)
        
    y_true, y_pred, moni_features_all = prediction_and_features(model, data_loader, device, 
                                                                moni_layers=[moni_layer], n=n, 
                                                                seed=seed)
    moni_features = moni_features_all[moni_layer]
    verdicts = -np.ones((len(y_true)), dtype=np.int8)

    # classify the features by its true and predicted labels
    for i, y in enumerate(classes):
        # extract the indices of samples classified as y
        indices_prediction_y = np.where(y_pred == y)[0]
        # extract test features classified as class y
        test_features_class_y = list(zip(indices_prediction_y, moni_features[indices_prediction_y]))
        
        # import monitor for class y at layer i with different clustering parameter
        enriched_appendix = "_enriched" if enriched else ""
        monitor_common_path = monitor_storage_folder + "/Monitors" + enriched_appendix + "/monitor_for"
        
        if taus[i] == "enlarged":
            monitor_path = monitor_common_path + "_class_" + str(y) + "_at_layer_" + moni_layer + "_enlarged.pkl"
            
        else:
            tau = round(taus[i], ndigits=5) if taus[i] > 0.00001 else taus[i]
            monitor_path = monitor_common_path + "_class_" + str(y) + "_at_layer_" + moni_layer + "_tau_" + str(tau) + ".pkl"
            
            if not os.path.exists(monitor_path):
                # monitor construction
                if enriched:
                    os.system('python build_monitors.py --benchmark '+ benchmark + ' --moni_layers '+moni_layer+
                            ' --taus ['+str(tau)+'] --enrich ' + str(enriched))
                else:
                    os.system('python build_monitors.py --benchmark '+ benchmark + ' --moni_layers '+moni_layer+
                            ' --taus ['+str(tau)+']')
       
        with open(monitor_path, 'rb') as f:
            monitor = pickle.load(f)
        
        # monitor make verdicts
        verdicts_class_y = monitor.make_verdicts(test_features_class_y)
        if pos == 'id':
            verdicts_class_y = 1 - verdicts_class_y
        verdicts[indices_prediction_y] = verdicts_class_y

    return verdicts


def get_monitor_tpr_fpr(y_true, y_pred):
    # make y_true a boolean vector
    num_pos = np.count_nonzero(y_true)
    tp = np.count_nonzero(np.logical_and(y_true == y_pred, y_pred == 1))
    num_neg = len(y_true) - num_pos
    fp = np.count_nonzero(np.logical_and(y_true != y_pred, y_pred == 1))

    tpr = 0 if num_pos == 0 else tp / num_pos
    fpr = 0 if num_neg == 0 else fp / num_neg

    return tpr, fpr

# Find tau@TPR=95 and FPR@TPR=95 using dichotomy
def tune_monitor(model, in_loader, logger, args, num_classes, device, enriched=False):

    # switch to evaluate mode
    model.eval()
    if enriched:
        logger.info(f"Tuning refined monitor in layer {args.monitor.layer}...")
    else:
        logger.info(f"Tuning monitor in layer {args.monitor.layer}...")
    logger.flush()

    classes = range(num_classes)
    taus_all = []
    tprs_all = []
    for y in classes:
        print('class:', y)  
        new_taus = [round(t, ndigits=5) if t > 0.00001 else t for t in args.monitor.taus_init]
        new_taus = [round(t) if t > 1 else t for t in new_taus]
        idx = range(len(new_taus))
        taus, tprs = np.empty(0), np.empty(0)

        for iter in range(20):
            
            for id, tau in zip(idx, new_taus):

                n_samples = cluster_existed_features([y], args.monitor.layer, [tau], 
                                                     args.monitor.storage_folder, 
                                                     enriched=enriched)
                monitors_offline_construction(args.benchmark, [y], args.monitor.layer, [tau], 
                                              args.monitor.storage_folder, enriched=enriched)
            
                in_verdicts = iterate_data_monitor(in_loader, model, device, args.benchmark, args.monitor.layer, 
                                                    args.monitor.storage_folder, [y], [tau], pos=args.eval.pos, 
                                                    enriched=enriched)

                                    
                in_verdicts = in_verdicts[np.where(in_verdicts != -1)[0]]

                y_pred = in_verdicts
                y_true = np.ones(len(in_verdicts)) # id as positive
                tpr, _ = get_monitor_tpr_fpr(y_true, y_pred)  # for class y

                tprs = np.insert(tprs, id, tpr)
                taus = np.insert(taus, id, tau)
                
                print("tprs", tprs)
                print("taus:", taus)

            if np.min(abs(tprs - args.monitor.target_tpr)) < 0.002: # error=0.01
                id = np.argmin(abs(tprs - args.monitor.target_tpr)) # find the closest tau@TPR=95%
                taus_all.append(taus[id])
                tprs_all.append(round(tprs[id]*100, 2))
                print('taus:', taus)
                print('tprs:', tprs)
                break

            # update idx and taus to insert into the list
            tpr_diff = (tprs - args.monitor.target_tpr) > 0
            idx = np.where(tpr_diff[:-1] != tpr_diff[1:])[0]
            new_taus = [0.5 * (taus[i] + taus[i+1]) for i in idx]
            for i in range(len(idx)):
                idx[i] = idx[i] + i + 1 # index update after insertion

            if len(new_taus) == 0:
                diff = abs(tprs - args.monitor.target_tpr)
                rev_diff = diff[::-1]
                id = len(diff) - np.argmin(rev_diff) - 1

                if id == 0: 
                    idx = [0]
                    if taus[0] == 1.0:  # expand a tight box if the best tau=1.0
                        tau = 1.0
                        # find 95% closest good feature points to the monitor
                        logger.info("Enlarging monitor's good abstraction boxes...")
                        y_pred_test_all = torch.empty(0, dtype=torch.uint8).to(device)
                        features_test_all = torch.empty(0, dtype=torch.float32).to(device)
                        with torch.no_grad():
                            for data in in_loader:
                                X = data[0].to(device)
                                layer_list, out_list = model.forward_feature_list(X)
                                y_pred_test = torch.argmax(out_list[-1], axis=1).to(device)
                                features_test = out_list[int(args.monitor.layer.replace('minus_', '-'))]
                                
                                y_pred_test_all = torch.concat((y_pred_test_all, y_pred_test))
                                features_test_all = torch.concat((features_test_all, features_test), axis=0)
    
                        class_y_indexes = np.where((y_pred_test_all == y).cpu())[0]
                        features_test_class_y = features_test_all[class_y_indexes].cpu().numpy()
                        
                        # import monitor for class y at layer i with different clustering parameter
                        appendix = "_enriched" if enriched else ""
                        monitor_common_path = args.monitor.storage_folder + "/Monitors" + appendix + "/monitor_for"
                        monitor_path = monitor_common_path + "_class_" + str(y) + "_at_layer_" + args.monitor.layer \
                                        + "_tau_" + str(tau) + ".pkl"
                        if not os.path.exists(monitor_path):
                            # monitor construction
                            if enriched:
                                os.system('python build_monitors.py --benchmark '+args.benchmark+' --moni_layers ' \
                                            + args.monitor.layer + ' --taus '+str(tau)+' --enrich ' \
                                            + str(args.monitor.enrich))
                            else:
                                os.system('python build_monitors.py --benchmark '+args.benchmark+' --moni_layers ' \
                                            + args.monitor.layer + ' --taus '+str(tau))
                        
                        enriched_appendix = "_enriched" if enriched else ""
                        monitor_stored_folder_path = args.monitor.storage_folder + "/Monitors" + enriched_appendix + "/"
                        if not os.path.exists(monitor_stored_folder_path):
                            os.makedirs(monitor_stored_folder_path)
                        monitor_stored_path = monitor_stored_folder_path + "monitor_for_class_" \
                                            + str(y) + "_at_layer_" + args.monitor.layer + "_enlarged.pkl"

                        if 1:  # not os.path.exists(monitor_stored_path):   
                                
                            with open(monitor_path, 'rb') as f:
                                monitor = pickle.load(f)
                            
                            # increase by % in each dimension                         
                            good_box = monitor.good_ref[0]
                            
                            a = [ival[0] for ival in good_box.ivals]
                            b = [ival[1] for ival in good_box.ivals]
                            
                            A = np.tile(a, (len(features_test_class_y), 1))
                            B = np.tile(b, (len(features_test_class_y), 1))
                                                    
                            dist_mat = np.abs(np.minimum(np.minimum(features_test_class_y-A,  B-features_test_class_y), 
                                                    np.zeros(features_test_class_y.shape)))
                            dist_mat = dist_mat / (B-A) # scale the distance matrix
                            dist_arr = np.max(dist_mat, axis=1)
                            
                            sorted_indices = sorted(enumerate(dist_arr), key=lambda x: x[1])
                            dist_ranks = [index for index, _ in sorted_indices]
                            sorted_dist_arr = [value for _, value in sorted_indices]
                            
                            delta_ival = sorted_dist_arr[round(args.monitor.target_tpr * len(sorted_dist_arr))-1]

                            new_ivals = [[ival[0]-delta_ival*(ival[1]-ival[0]), ival[1]+delta_ival*(ival[1]-ival[0])] 
                                        for ival in good_box.ivals]
                            
                            good_box.ivals = new_ivals
                            
                            with open(monitor_stored_path, 'wb') as f:
                                pickle.dump(monitor, f)
                                
                        else:
                            with open(monitor_stored_path, 'rb') as f:
                                monitor = pickle.load(f)
                                
                        in_verdicts = iterate_data_monitor(in_loader, model, device, args.benchmark, args.monitor.layer, 
                                                            args.monitor.storage_folder, [y], ["enlarged"], 
                                                            pos=args.eval.pos,
                                                            enriched=enriched)   
                        in_verdicts = in_verdicts[np.where(in_verdicts != -1)[0]]
                        y_pred = in_verdicts
                        y_true = np.ones(len(in_verdicts)) # id as positive

                        tpr, fpr = get_monitor_tpr_fpr(y_true, y_pred)
                    
                        tprs = np.insert(tprs, 0, tpr)
                        taus = np.insert(taus, 0, 1.0)

                        taus_all.append("enlarged")
                        tprs_all.append(round(tpr*100, 2))
                        print('taus:', taus)
                        print("delta_ival =", delta_ival)
                        print('tprs:', tprs)

                        break
                        
                    else:
                        idx = [0]
                        new_taus = [taus[0] * 10]

                elif id == (len(taus)-1):
                    idx = [len(taus)]
                    
                    if taus[id] < 0.0001:  #TODO
                        n_cluster = round(min(taus[id] + np.sqrt(n_samples[0]), 
                                              n_samples[0]))
                        new_taus = [n_cluster]
                    elif taus[id] > 1:
                        n_cluster = round(min(taus[id] + np.sqrt(n_samples[0]), 
                                              n_samples[0]))
                        new_taus = [n_cluster]
                    else:
                        new_taus = [taus[id] * 0.1]
                else:
                    id = np.argmin(abs(tprs - args.monitor.target_tpr)) # find the closest tau@TPR=95%
                    taus_all.append(taus[id])
                    tprs_all.append(round(tprs[id]*100, 2))
                    print('taus:', taus)
                    print('tprs:', tprs)
                    break

            new_taus = [round(t, ndigits=5) if t > 0.00001 else t for t in new_taus]
            new_taus = [round(t) if t > 1 else t for t in new_taus]

            min_error = np.min(abs(tprs - args.monitor.target_tpr))
            count = np.count_nonzero(abs(tprs - args.monitor.target_tpr)-min_error < 0.001)
            if count > 3 or iter == 19:
                id = np.argmin(abs(tprs - args.monitor.target_tpr)) # find the closest tau@TPR=95%
                taus_all.append(taus[id])
                tprs_all.append(round(tprs[id]*100, 2))
                print('class:', y)  
                print('taus:', taus)
                print('tprs:', tprs)
                break

    logger.info(f"taus_all: {taus_all}")
    logger.info(f"tprs_all: {tprs_all}")

    return taus_all, tprs_all

    
def sort_clusters_by_size(monitors, show_distribution=False):
    
    all_boxes = [] # (class, list of boxes)
    for monitor in monitors: # from 0 to 9
        for box in monitor.good_ref:
            all_boxes.append((monitor.classification, 'good', box))
        for box in monitor.bad_ref:
            all_boxes.append((monitor.classification, 'bad', box))

    all_boxes_sorted_by_size = sorted(all_boxes, key=lambda d: len(d[2].element_indexes))
    all_sizes = [len(box[2].element_indexes) for box in all_boxes_sorted_by_size]
    
    if show_distribution:

        fig = plt.figure(figsize=(300,100))
        axes = fig.subplots(1, 3)

        good_boxes_sorted_by_size = [box for box in all_boxes_sorted_by_size if box[1]=='good']
        good_sizes = [len(box[2].element_indexes) for box in good_boxes_sorted_by_size]
        bad_boxes_sorted_by_size = [box for box in all_boxes_sorted_by_size if box[1]=='bad']
        bad_sizes = [len(box[2].element_indexes) for box in bad_boxes_sorted_by_size]

        ax = axes[0]
        count, bins, _ = ax.hist(all_sizes, bins=25)
        ax.set_title("Box size distribution")
        ax.set_xlabel("box size")
        ax.set_ylabel("frequency")

        ax = axes[1]
        count, bins, _ = ax.hist(good_sizes, bins=25)
        ax.set_title("Good box size distribution")
        ax.set_xlabel("box size")
        ax.set_ylabel("frequency")

        ax = axes[2]
        count, bins, _ = ax.hist(bad_sizes, bins=25)
        ax.set_title("Bad box size distribution")
        ax.set_xlabel("box size")
        ax.set_ylabel("frequency")
        
        plt.show()

    return all_boxes_sorted_by_size, all_sizes

# Load original monitors
def load_monitors(n_classes, logger, args):
    monitor_common_path = args.monitor.storage_folder + "/Monitors/monitor_for"
    tau = args.monitor.tau
    tau = round(tau, ndigits=5) if tau > 0.00001 else args.monitor.tau
    monitors_origin = []
    for y in range(n_classes):
        if args.monitor.enlarged:
            monitor_path = monitor_common_path + "_class_" + str(y) + "_at_layer_" + args.monitor.layer \
                            + "_enlarged.pkl"
        else:
            monitor_path = monitor_common_path + "_class_" + str(y) + "_at_layer_" + args.monitor.layer \
                            + "_tau_" + str(tau) + ".pkl"
        # if not os.path.exists(monitor_path):
        #     # monitor construction
        #     os.system('python build_monitors.py --benchmark '+args.benchmark+' --moni_layers '+args.monitor.layer
        #               +' --taus '+str(tau))
        with open(monitor_path, 'rb') as f:
            monitor = pickle.load(f)
            logger.info('Monitor loaded from ' + monitor_path)
            monitors_origin.append(monitor)

    return monitors_origin

def refine_monitors(monitors_origin, model, train_dataset_raw, device, logger, args):
    
    # to save generated feature data in csv files
    storage_folder = args.monitor.storage_folder + '/moni_features_enriched/Layer_' + args.monitor.layer + '/'
    if not os.path.exists(storage_folder):
        os.makedirs(storage_folder)

    tfs = tv.transforms.Compose([tv.transforms.Resize((args.preprocess.input_size, args.preprocess.input_size), antialias=True),
                                 tv.transforms.Normalize(args.preprocess.mean, args.preprocess.std)])
    
    all_boxes_sorted, all_sizes = sort_clusters_by_size(monitors_origin, show_distribution=False)

    temp_idxes_all = np.empty(0, dtype=np.uint32)
    temp_y_true_labels_all = np.empty(0, dtype=np.uint8)
    temp_y_pred_labels_all = np.empty(0, dtype=np.uint8)
    temp_features_all = []
    
    # collect monitored features and model predictions of original samples (read from csv files)
    for y in range(len(train_dataset_raw.classes)):
        for appendix in ['incorrect', 'correct']:
            y_features_file_path = args.monitor.storage_folder+'/moni_features/Layer_'+args.monitor.layer+'/'+'class_'+str(y) \
                                    +'_'+appendix+'ly_classified_features.csv'

            df = pd.read_csv(y_features_file_path,  dtype={"index": str})
            index = df['index'].to_numpy()
            true_label =df['true_label'].to_numpy()
            pred_label = df['pred_label'].to_numpy()
            features = []
            for col in df.columns:
                if col[0] == 'n':
                    features.append(df[col].to_numpy(dtype=np.float32))
            features = np.stack(features, axis=1)

            temp_idxes_all = np.concatenate((temp_idxes_all, index))
            temp_y_true_labels_all = np.concatenate((temp_y_true_labels_all, true_label))
            temp_y_pred_labels_all = np.concatenate((temp_y_pred_labels_all, pred_label))
            temp_features_all.append(features)
    
    temp_features_all = np.row_stack(temp_features_all)
        
    for y in range(len(train_dataset_raw.classes)):
        
        gen_idxes_class_y = np.empty(0)
        gen_data_class_y = torch.empty(0, dtype=torch.float32)
        gen_y_true_labels_class_y = torch.empty(0, dtype=torch.uint8)
        gen_y_pred_labels_class_y = torch.empty(0, dtype=torch.uint8)
        gen_features_class_y = torch.empty(0, dtype=torch.float32)
                
        class_y_correct_indexes = np.where(np.logical_and(temp_y_pred_labels_all == y, 
                                                        temp_y_pred_labels_all == temp_y_true_labels_all))[0]
        class_y_incorrect_indexes = np.where(np.logical_and(temp_y_pred_labels_all == y, 
                                                            temp_y_pred_labels_all != temp_y_true_labels_all))[0]

        for appendix, class_y_indexes in {'incorrect': class_y_incorrect_indexes, 
                                          'correct': class_y_correct_indexes}.items():
            
            feature_file_path = storage_folder + 'class_' + str(y) + '_' + appendix + 'ly_classified_features.csv'
            if os.path.exists(feature_file_path):
                continue

            temp_idx_class_y = temp_idxes_all[class_y_indexes]
            temp_y_true_labels_class_y = temp_y_true_labels_all[class_y_indexes]
            temp_y_pred_labels_class_y = temp_y_pred_labels_all[class_y_indexes]
            temp_features_class_y = temp_features_all[class_y_indexes]
            
            logger.info(f"Enriching clusters of {appendix}ly classified features of class_{y}. Number of samples: {len(temp_idx_class_y)}.")
        
            for temp_id in temp_idx_class_y:
                
                X_0 = train_dataset_raw[temp_id][0]
                y_0 = train_dataset_raw[temp_id][1]

                # generate n_gen samples from each template sample
                resample_i = 0
                max_resample = 100
                gen_data = torch.empty(0, dtype=torch.float32)

                while len(gen_data) < args.sampling.n_gen:
                    
                    # if the majority of generated samples fall outside the neighborhood of original samples in feature space
                    if resample_i > max_resample:
                        # logger.info(f'temp_id: {temp_id} resample_i > {max_resample}! temp_y_pred={temp_y_pred}, gen_y_pred={gen_y_pred}, n_gen={len(gen_data)}')
                        break
                    X_gen = X_0.clone().reshape(-1)

                    if args.sampling.method.name == 'pixel':
                        num_px = args.sampling.method.n_px
                        # change 2 pixels
                        px_id = random.sample(range(0, len(X_gen)), num_px)
                        px_val = torch.randint(0, 256, (num_px,))
                        X_gen[px_id] = px_val
                            
                    elif args.sampling.method.name == 'gaussian':
                        # Define the mean and covariance matrix for the Gaussian distribution
                        std = args.sampling.method.std
                        X_gen += (torch.randn(X_gen.shape) * std).int()
                        X_gen = torch.clamp(X_gen, 0, 255)  # Clip the tensor between 0 and 255
                    
                    elif args.sampling.method.name == 'uniform':
                        a = args.sampling.method.a
                        noise = torch.randint(-a, a, X_gen.shape)
                        X_gen += noise
                        X_gen = torch.clamp(X_gen, 0, 255) # Clip the tensor between 0 and 255
                        
                    # compute euclidean distance w.r.t. all original sample distance distribution
                    dist_to_origin = pairwise_distances(X=X_gen.unsqueeze(0), Y=X_0.reshape(-1).unsqueeze(0), 
                                                        metric='euclidean').reshape(-1)
                    if len(gen_data) > 0:
                        dist_to_gens = pairwise_distances(X=gen_data.reshape(len(gen_data),-1), Y=X_gen.unsqueeze(0),
                                                            metric='euclidean').reshape(-1)
                        min_dist_to_gens = np.min(dist_to_gens)
                        # eliminate repeated samples
                        if min_dist_to_gens < 400: # TODO
                            logger.info(f'Repeated sample eliminated. Distance={min_dist_to_gens}')
                            continue
                
                    # the same ground truth
                    if float(dist_to_origin) < args.sampling.theta:
                        # reshape & normalize generated data
                        X_gen = X_gen.reshape(X_0.shape)
                        X_gen_tf = tfs((X_gen.float()/255)).to(device)
                        
                        # collect network's outputs and predictions of the generated samples' inputs
                        model.eval()
                        with torch.no_grad():  
                            layer_list, out_list = model.forward_feature_list(X_gen_tf.unsqueeze(0))
                            gen_y_pred = torch.argmax(out_list[-1]).item()
                            gen_features = out_list[int(args.monitor.layer.replace('minus_', '-'))].cpu()

                        # the same predicted label
                        if gen_y_pred == y:

                            gen_idxes_class_y = np.concatenate((gen_idxes_class_y, np.array([f'{temp_id}_{len(gen_data)}'])))
                            gen_data = torch.concat((gen_data, X_gen.unsqueeze(0)))
                            
                            gen_y_true_labels_class_y = torch.concat((gen_y_true_labels_class_y, torch.tensor(y_0).unsqueeze(0)))
                            gen_y_pred_labels_class_y = torch.concat((gen_y_pred_labels_class_y, torch.tensor(gen_y_pred).unsqueeze(0)))
                            gen_features_class_y = torch.concat((gen_features_class_y, gen_features),)
                    
                    resample_i += 1
                    
                gen_data_class_y = torch.concat((gen_data_class_y, gen_data))
            
            
            neuron_names = ["n" + str(i) for i in range(temp_features_class_y.shape[1])]
            temp_idx_class_y = [str(id) for id in temp_idx_class_y]
            idxes_class_y = np.concatenate((temp_idx_class_y, gen_idxes_class_y))
            y_pred_labels_class_y = np.concatenate((temp_y_pred_labels_class_y, gen_y_pred_labels_class_y.numpy()))
            y_true_labels_class_y = np.concatenate((temp_y_true_labels_class_y, gen_y_true_labels_class_y.numpy()))
            features_class_y = np.concatenate((temp_features_class_y, gen_features_class_y.numpy())) if len(gen_features_class_y) > 0 else temp_features_class_y
            
            # save features into a csv file
            
            logger.info(f"Saving {len(idxes_class_y)} generated features to {feature_file_path}...")
            df = pd.DataFrame(data=features_class_y, columns=neuron_names)
            df.insert(0, "pred_label", y_pred_labels_class_y)
            df.insert(0, "true_label", y_true_labels_class_y)
            df.insert(0, "index", idxes_class_y)
            df.to_csv(feature_file_path, index = False)
             
    # feature clustering & tune monitor to obtain tau@TPR=95%
    in_set, out_set_list, in_loader, out_loader_list = make_id_ood(args, logger)
    tau, tpr = tune_monitor(model, in_loader, logger, args, 
                                        len(in_set.classes), device, enriched=True)
    logger.info(f"tau={str(tau)}, tpr={str(tpr)}")
    
