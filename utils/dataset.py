from torch.utils.data import Dataset
import torch

class CustomDataset(Dataset):
    def __init__(self, data_labels, transform=None):
        self.data_labels = data_labels
        self.transform = transform

    def __len__(self):
        return len(self.data_labels)

    def __getitem__(self, idx):
        sample = list(self.data_labels[idx])

        if self.transform:
            sample[0] = self.transform(sample[0].to(torch.float32))

        return sample
