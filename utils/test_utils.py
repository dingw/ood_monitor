import yaml
import argparse
import numpy as np
import random
import torch
from tkinter import messagebox, simpledialog, Tk

def setup_seed(seed):
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    np.random.seed(seed)
    random.seed(seed)
    torch.backends.cudnn.deterministic = True


## define custom tag handler
def join(loader, node):
    seq = loader.construct_sequence(node)
    return ''.join([str(i) for i in seq])

def join_lower(loader, node):
    seq = loader.construct_sequence(node)
    return ''.join([str(i).lower() for i in seq])

def read_yaml_file(file_path):
    ## register the tag handler
    yaml.SafeLoader.add_constructor('!join', join)
    yaml.SafeLoader.add_constructor('!join_lower', join_lower)
    with open(file_path, 'r') as file:
        return yaml.safe_load(file)

# def prompt_user_inputs(default_args):
#     root = Tk()
#     root.withdraw()  # Hide the main window
#     user_args = {}

#     for arg, default_value in default_args.items():
#         user_input = simpledialog.askstring("User Input", f"Enter {arg} (default: {default_value}):",
#                                             initialvalue=default_value)
#         if user_input is not None:
#             user_args[arg] = user_input

#     return user_args

# def your_function(name, age, city):
#     # Replace this with your actual Python function that uses the user inputs
#     # For example, you could perform some calculations based on the inputs and return a result
#     return age * 2

# def submit_button_clicked(name, age, city):
#     if name and age:
#         try:
#             age = int(age)
#             # Call your Python function with the user inputs
#             result = your_function(name, age, city)

#             # Display the result in a message box
#             messagebox.showinfo("Result", f"Hello, {name}! You are {age} years old and from {city}.\nFunction result: {result}")
#         except ValueError:
#             messagebox.showerror("Error", "Age must be an integer.")
#     else:
#         messagebox.showerror("Error", "Please enter your name and age.")

def replace_string_recursive(data, target, replacement):
    if isinstance(data, str):
        return data.replace(target, replacement, -1)
    elif isinstance(data, dict):
        for key, value in data.items():
            data[key] = replace_string_recursive(value, target, replacement)
        return data
    elif isinstance(data, list):
        return [replace_string_recursive(item, target, replacement) for item in data]
    else:
        return data
    
def update_configs(configs, args):
    if args.benchmark is not None:
        default_bm = configs['benchmark']
        replace_string_recursive(configs, default_bm, args.benchmark)
        replace_string_recursive(configs, default_bm.lower(), args.benchmark.lower())

    for key, value in vars(args).items():
        if value is None:
            continue
        else:
            keys = key.split('/')
            sub_dict = configs
            for k in keys:
                if k == keys[-1]:
                    sub_dict[k] = value
                else:
                    if k in sub_dict:
                        pass
                    else:
                        sub_dict[k] = dict()
                sub_dict = sub_dict[k]

class HierarchicalDict:
    def __init__(self, data):
        self._data = data

    def __getattr__(self, name):
        if name in self._data:
            value = self._data[name]
            if isinstance(value, dict):
                return HierarchicalDict(value)
            return value
        else:
            raise AttributeError(f"'{type(self).__name__}' object has no attribute '{name}'")
            
    def __iter__(self):
        return iter(self._data)

    def keys(self):
        return self._data.keys()

    def values(self):
        return self._data.values()

    def items(self):
        return self._data.items()
    

def parse_args(configs):
    # Read input arguments from the YAML file
    configs = read_yaml_file(configs)

    # Parse the --use_panel argument using argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--use_panel", action="store_true", help="Prompt GUI panel for input")
    args, unknown = parser.parse_known_args()

    if args.use_panel:
        pass  #TODO
        # # Prompt the user to input all arguments using GUI
        # user_args = prompt_user_inputs(configs)
        # # Replace default values with user-input values
        # configs.update(user_args)
        # # Call the submit_button_clicked function with the user inputs
        # submit_button_clicked(name, age, city) 
    else:
        # Read additional command-line arguments using argparse
        parser = argparse.ArgumentParser()

        # benchmark
        parser.add_argument("--benchmark", type=str, dest='benchmark', choices=['CIFAR10', 'CIFAR100'])
        # preprocess
        group_name = 'preprocess/'
        parser.add_argument("--workers", type=int, dest=group_name+'workers',
                            help="Number of background threads used to load data.")
        parser.add_argument("--batch_size", type=int, dest=group_name+'batch_size',
                            help="Batch size.")
        parser.add_argument("--input_size", type=int, dest=group_name+'input_size', help="Input size of the model" )
        parser.add_argument('--mean', nargs='+', type=float, dest=group_name+'mean')
        parser.add_argument('--std', nargs='+', type=float, dest=group_name+'std')
        
        group_name = 'model/'
        parser.add_argument("--model_name", type=str, dest=group_name+'name', help="Which model family to use")
        parser.add_argument("--depth", type=int, dest=group_name+'depth')
        parser.add_argument("--widden_factor", type=float, dest=group_name+'widden_factor')
        parser.add_argument("--model_path", type=str, dest=group_name+'path', help="Path to the model you want to test")
        parser.add_argument("--droprate", type=float, dest=group_name+'droprate', help="Drop rate for WideResNet moodel")
        
        group_name = 'data/'
        parser.add_argument('--in_data', type=str, dest=group_name+'id/name', help="Name of ID dataset.")
        parser.add_argument("--in_datadir", type=str, dest=group_name+'id/folder', help="Path to the in-distribution test data folder.")
        parser.add_argument('--out_data', nargs='+', type=str, dest=group_name+'ood/name', help="Names of OoD datasets.")
        parser.add_argument("--out_datadir", type=str, dest=group_name+'ood/folder', help="Path to the out-of-distribution data folder.")

        group_name = 'eval/'
        parser.add_argument("--num_ood", type=int, dest=group_name+'num_ood', help="Number of OoD samples taken for the test.")
        parser.add_argument("--seed", dest=group_name+'seed', type=int)
        parser.add_argument("--num_to_avg", dest=group_name+'num_to_avg', type=int)
        parser.add_argument("--pos", dest=group_name+'pos', type=str)

        group_name = 'monitor/'
        parser.add_argument("--moni_layer", dest=group_name+'layer', type=str)
        parser.add_argument("--tau", dest=group_name+'tau', type=float)
        parser.add_argument("--moni_layers", nargs='+', dest=group_name+'layers', type=str)
        parser.add_argument("--taus", nargs='+', dest=group_name+'taus', type=float)
        parser.add_argument("--taus_init", nargs='+', dest=group_name+'taus_init', type=float)
        parser.add_argument("--target_tpr", dest=group_name+'target_tpr', type=float)
        parser.add_argument("--monitor_storage_folder", dest=group_name+'storage_folder', type=str)
        parser.add_argument("--N_min", dest=group_name+'N_min', type=int)
        parser.add_argument("--enrich", dest=group_name+'enrich', type=bool)
        
        group_name = 'scores/'
        # TODO
        # parser.add_argument("--scores", nargs='+', dest=group_name+'names', type=str, 
        #                     choices=['MSP', 'ODIN', 'Energy', 'Mahalanobis', 'GradNorm'])
        parser.add_argument("--temperature_msp", dest=group_name+'msp/T', type=float,
                            help='temperature scaling for MSP')
        parser.add_argument("--temperature_odin", dest=group_name+'odin/T', type=int,
                            help='temperature scaling for ODIN')
        parser.add_argument("--epsilon_odin", dest=group_name+'odin/epsilon', type=float,
                            help='perturbation magnitude for ODIN')
        parser.add_argument("--temperature_energy", dest=group_name+'energy/T', type=float,
                            help='temperature scaling for Energy')
        parser.add_argument("--temperature_gradnorm", dest=group_name+'gradnorm/T', type=float,
                            help='temperature scaling for GradNorm')

        parser.add_argument("--logdir", type=str, dest='logdir',
                            help="Where to log test info (small).")
        
        group_name = 'sampling/'
        parser.add_argument("--n_temp", dest=group_name+'n_temp', type=int)
        parser.add_argument("--n_gen", dest=group_name+'n_gen', type=int)
        parser.add_argument("--method", dest=group_name+'method', type=str)
        parser.add_argument("--theta", dest=group_name+'theta', type=float)
        
        args = parser.parse_args()

        update_configs(configs, args)

        return HierarchicalDict(configs)

def stable_cumsum(arr, rtol=1e-05, atol=1e-08):
    """Use high precision for cumsum and check that final value matches sum
    Parameters
    ----------
    arr : array-like
        To be cumulatively summed as flat
    rtol : float
        Relative tolerance, see ``np.allclose``
    atol : float
        Absolute tolerance, see ``np.allclose``
    """
    out = np.cumsum(arr, dtype=np.float64)
    expected = np.sum(arr, dtype=np.float64)
    if not np.allclose(out[-1], expected, rtol=rtol, atol=atol):
        raise RuntimeError('cumsum was found to be unstable: '
                           'its last element does not correspond to sum')
    return out
