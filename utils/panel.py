# Import libraries
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE

from matplotlib.colors import ListedColormap
from mpl_toolkits.mplot3d import Axes3D

from sklearn.metrics import pairwise_distances
from distribution import CustomMultivariateKDE, CustomMultivariateUniform, cal_kl_div
from sklearn.neighbors import NearestNeighbors

PALETTE = sns.color_palette('deep', n_colors=3)
CMAP = ListedColormap(PALETTE.as_hex())

def plot_data_2d(x, y, c, title, ax):
    sns.set_style("darkgrid")
    ax.scatter(x, y, c=c, cmap=CMAP, s=1)

    fsize = 10
    ax.set_title(title, fontsize=fsize, y=1.03)
    ax.set_xlabel("1st eigenvector", fontsize=fsize)
    ax.set_ylabel("2nd eigenvector", fontsize=fsize)

def plot_data_3d(x, y, z, c, title, ax):
    sns.set_style('whitegrid')
    ax.scatter3D(x, y, z, c=c, cmap=CMAP, s=1)

    fsize = 10
    ax.set_title(title, fontsize=fsize, y=1.03)
    ax.set_xlabel("1st eigenvector", fontsize=fsize)
    ax.set_ylabel("2nd eigenvector", fontsize=fsize)
    ax.set_zlabel("3rd eigenvector", fontsize=fsize)
    
    ax.w_xaxis.set_ticklabels([])
    ax.w_yaxis.set_ticklabels([])
    ax.w_zaxis.set_ticklabels([])

def display_distribution(temp_data, gen_data, idx_origin, layer, logger, args):
 
    fig = plt.figure(figsize=(600, 400))
    axes = fig.subplots(2,3)
    plt.subplots_adjust(bottom=0.35)
    fsize = 8
    data_origin = np.stack([temp_data[id] for id in idx_origin], axis=0)
    # Euclidean distance distribution plot
    pos_rel = gen_data.reshape(len(gen_data), -1) - data_origin.reshape(len(data_origin), -1)
    dist_rel = pairwise_distances(X=pos_rel, Y=np.zeros((1, pos_rel.shape[1])), 
                                       metric='euclidean').reshape(-1)   
    
    sns.kdeplot(np.array(dist_rel), bw_adjust=1, ax=axes[0,0])
    axes[0,0].set_title('Eucliden distance distribution between generated noisy samples and template samples in '+layer+' layer.',
                      fontsize=fsize)
    # k-NN distance distribution plot
    k = 10
    logger.info("Calculating k-NN distance")
    knn_model = NearestNeighbors(n_neighbors=k, metric='euclidean')
    knn_model.fit(pos_rel)
    distances, indices = knn_model.kneighbors(pos_rel)

    k_dist = distances[:, k-1]
    sns.kdeplot(k_dist, bw_adjust=1, ax=axes[1,0])
    axes[1,0].set_title('k-NN distance distribution plot of generated noisy samples in '+layer+' layer. (k=10)',
                          fontsize=fsize)
    
    # Create a slider from 0.0 to 10.0 in axes axred with 1.0 as initial value.
    ax_bw_adjust1 = plt.axes([0.25, 0.2, 0.65, 0.03])
    bw_adjust1 = Slider(ax_bw_adjust1, 'bandwidth_euclidean_dist', 0.0, 5.0, 1.0)
    ax_bw_adjust2 = plt.axes([0.25, 0.1, 0.65, 0.03])
    bw_adjust2 = Slider(ax_bw_adjust2, 'bandwidth_knn_dist', 0.0, 5.0, 1.0)
    # Create axes for reset button and create button
    resetax = plt.axes([0.8, 0.025, 0.1, 0.04])
    button = Button(resetax, 'Reset', color='gold',
                    hovercolor='skyblue')
    
    # Create function to be called when slider value is changed
    def bw_adjust1_update(val):
        bw = bw_adjust1.val
        axes[0,0].clear()
        sns.kdeplot(np.array(dist_rel), bw_adjust=bw, ax=axes[0,0])
        axes[0,0].set_title('Distribution of Eucliden distance between generated noisy samples and template samples in '+layer+' layer.',
                          fontsize=fsize)
        
    def bw_adjust2_update(val):
        bw = bw_adjust2.val
        axes[1,0].clear()
        sns.kdeplot(np.array(k_dist), bw_adjust=bw, ax=axes[1,0])
        axes[1,0].set_title('k-NN distance distribution plot of generated noisy samples in '+layer+' layer (k=10).',
                          fontsize=fsize)
    # Create a function resetSlider to set slider to
    # initial values when Reset button is clicked
    def resetSlider(event):
        bw_adjust1.reset()
        bw_adjust2.reset()

    # Call update function when slider value is changed
    bw_adjust1.on_changed(bw_adjust1_update)
    bw_adjust2.on_changed(bw_adjust2_update)
    # Call resetSlider function when clicked on reset button
    button.on_clicked(resetSlider)

    # Distribution analysis with pos_rel_list
    pca_2d = PCA(n_components=2)
    tsne_2d = TSNE(n_components=2, n_iter=1000, init='pca', random_state=args.seed, learning_rate='auto')
    points = pca_2d.fit_transform(pos_rel)
    plot_data_2d(x=points[:,0],y=points[:,1], c=np.zeros(len(dist_rel)), 
                 title='Relative distribution of template and generated noisy dataset (PCA)', ax=axes[0,1])
    points = tsne_2d.fit_transform(pos_rel)
    plot_data_2d(x=points[:,0],y=points[:,1], c=np.zeros(len(dist_rel)), 
                 title='Relative distribution of template and generated noisy dataset (t-SNE)', ax=axes[0,2])

    # panorama
    data = np.concatenate((temp_data, gen_data))
    data = data.reshape(len(data), -1)
    c = np.concatenate((np.zeros(len(temp_data)), np.ones(len(gen_data))))

    # points = pca_2d.fit_transform(data)
    # plot_data_2d(x=points[:,0],y=points[:,1], c=c, 
    #              title='Template and generated noisy dataset visualized with PCA', ax=axes[1])

    # points = tsne_2d.fit_transform(data)
    # plot_data_2d(x=points[:, 0], y=points[:, 1], c=c, 
    #              title='Template and generated noisy dataset visualized with t-SNE', ax=axes[2])

    pca_3d = PCA(n_components=3)
    points = pca_3d.fit_transform(data)
    ax = fig.add_subplot(2, 3, 5, projection='3d')
    plot_data_3d(x=points[:,0], y=points[:,1], z=points[:,2], c=c, 
                 title="Panorama of template and generated noisy dataset (PCA)", ax=ax)

    tsne_3d = TSNE(n_components=3, n_iter=5000, init='pca', random_state=args.seed, learning_rate='auto')
    points = tsne_3d.fit_transform(data)
    ax = fig.add_subplot(2, 3, 6, projection='3d')
    plot_data_3d(x=points[:,0], y=points[:,1], z=points[:,2], c=c, 
                 title="Panorama of template and generated noisy dataset (t-SNE)", ax=ax)

    # # multivarite KL divergence conparing with uniform distribution
    # dimensions = 3
    # pca_nd = PCA(n_components=dimensions)
    # points = pca_nd.fit_transform(pos_rel_list)
    # pos_rel_normalized = (points - np.min(points)) / (np.max(points) - np.min(points))

    # pos_rel_distri = CustomMultivariateKDE(pos_rel_normalized, bandwidth=0.01)
    # uniform_distri = CustomMultivariateUniform(torch.zeros(dimensions, dtype=float), torch.ones(dimensions, dtype=float))
    # kl_divergences = cal_kl_div(pos_rel_distri, uniform_distri, dimensions=dimensions)
    
    plt.show()
    plt.close()



