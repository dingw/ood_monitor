import torch
import time

from utils import log
from utils.test_utils import parse_args
from utils.dataloader import load_dataset_from_torchvision
from utils.monitor import refine_monitors, load_monitors, sort_clusters_by_size
from models.model_utils import build_model
import numpy as np

def main(args):
    # Set device
    logger = log.setup_logger(args)
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    torch.backends.cudnn.benchmark = True

    # Load training datasets data_dir, dataset_name, args, split="", shuffle=False, raw=False
    train_dataset_raw, train_dataloader_raw = load_dataset_from_torchvision(args.data.id.folder, 
                                                                            args.data.id.name, "train", 
                                                                            args, shuffle=False, 
                                                                            raw=True)

    # Build model
    logger.info(f"Loading model from {args.model.path}")
    model = build_model(args, len(train_dataset_raw.classes), device)

    # Load original monitors and search for small clusters
    monitors_origin = load_monitors(len(train_dataset_raw.classes), logger, args)
    all_boxes_sorted, all_sizes = sort_clusters_by_size(monitors_origin, show_distribution=True)
    logger.info(f'Total number of clusters: {len(all_boxes_sorted)}')
    logger.info(f'Number of clusters smaller than {args.monitor.N_min}: {np.count_nonzero(np.array(all_sizes) <= args.monitor.N_min)}')

    # Refine monitors in the user-defined layer & clustering parameter tau
    refine_monitors(monitors_origin, model, train_dataset_raw, device, logger, args)


if __name__ == "__main__":
    
    args = parse_args(configs='configs/refine_monitor_config.yaml')

    main(args)
