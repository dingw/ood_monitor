import torch
import time

from utils import log
from utils.test_utils import parse_args
from utils.eval import run_eval
from utils.dataloader import load_dataset_from_torchvision
from utils.monitor import sort_class_wise_features, prediction_and_features, cluster_existed_features, monitors_offline_construction
from models.model_utils import build_model
import torchvision as tv

def main(args):
    # Set device
    logger = log.setup_logger(args)
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    torch.backends.cudnn.benchmark = True

    # Load training datasets 
    train_dataset, train_dataloader = load_dataset_from_torchvision(args.data.id.folder, args.data.id.name, "train", 
                                                    args, shuffle=False)
    classes = range(len(train_dataset.classes))
    
    # Build model
    logger.info(f"Loading model from {args.model.path}")
    model = build_model(args, len(train_dataset.classes), device)

    if not args.monitor.enrich:
        # Feature extraction
        start_time = time.time()
        y_train, y_train_pred, y_train_moni_features = prediction_and_features(model, train_dataloader, device, 
                                                                            moni_layers=[args.monitor.layer])
        sort_class_wise_features(y_train, y_train_pred, y_train_moni_features, args.monitor.storage_folder)
        end_time = time.time()
        logger.info("Feature extraction time: {}".format(end_time - start_time))


    # Feature clustering
    start_time = time.time()
    cluster_existed_features(classes, args.monitor.layer, args.monitor.taus, 
                             args.monitor.storage_folder, enriched=args.monitor.enrich)
    end_time = time.time()
    logger.info("Feature clustering time: {}".format(end_time - start_time))
    
    
    # Monitor construction
    start_time = time.time()
    monitors_offline_construction(args.benchmark, classes, args.monitor.layer, args.monitor.taus, 
                                  args.monitor.storage_folder, enriched=args.monitor.enrich)
    end_time = time.time()
    logger.info("Monitor construction time: {}".format(end_time - start_time))

if __name__ == "__main__":
    
    args = parse_args(configs='configs/build_monitors_config.yaml')

    main(args)
