{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## **Implementation of OoD detection methods**\n",
    "This notebook aims to demonstrate our implementation of different OoD detection methods, including MSP, ODIN, Energy and GradNorm score functions, as well as LogitNorm loss function for the training process. We use CIFAR10 benchmark with WRN-40-2 model, using CIFAR-10 test dataset as in-distribution dataset and Textures as out-of-distribution dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "/home/dingw/work/ood_benchmark\n",
      "Device: cuda\n",
      "Loading model WRN-40-2 from checkpoints/cifar10_wrn_normal_standard_epoch_199.pt\n",
      "Loading in-distribution dataset CIFAR-10...\n",
      "Loading out-of-distribution dataset Textures...\n"
     ]
    }
   ],
   "source": [
    "import os\n",
    "os.chdir(os.getcwd()+\"/..\")\n",
    "\n",
    "import torch\n",
    "from torch.autograd import Variable\n",
    "import torchvision as tv\n",
    "import numpy as np\n",
    "\n",
    "from utils.test_utils import setup_seed\n",
    "import models.wrn as wrn\n",
    "\n",
    "# Set device\n",
    "device = torch.device(\"cuda\") if torch.cuda.is_available() else torch.device(\"cpu\")\n",
    "print('Device:', device)\n",
    "torch.backends.cudnn.benchmark = True\n",
    "\n",
    "# Build model\n",
    "print(\"Loading model WRN-40-2 from checkpoints/cifar10_wrn_normal_standard_epoch_199.pt\")\n",
    "state_dict = torch.load(\"./checkpoints/cifar10_wrn_normal_standard_epoch_199.pt\")\n",
    "model = wrn.KNOWN_MODELS[\"WRN-40-2\"](head_size=10, dropRate=0.3)\n",
    "model.load_state_dict(state_dict)\n",
    "model = model.to(device)\n",
    "\n",
    "# Load dataset\n",
    "test_tx = tv.transforms.Compose([\n",
    "                tv.transforms.Resize((32, 32)),\n",
    "                tv.transforms.ToTensor(),\n",
    "                tv.transforms.Normalize((0.492, 0.482, 0.446), (0.247, 0.244, 0.262)),\n",
    "                ])\n",
    "print(\"Loading in-distribution dataset CIFAR-10...\")\n",
    "in_set = tv.datasets.ImageFolder(\"dataset/id_data/CIFAR10\", test_tx)\n",
    "in_loader = torch.utils.data.DataLoader(\n",
    "    in_set, batch_size=20, shuffle=False,\n",
    "    num_workers=0, pin_memory=True, drop_last=False)\n",
    "print(\"Loading out-of-distribution dataset Textures...\")\n",
    "out_set = tv.datasets.ImageFolder(\"dataset/ood_data/Textures\", test_tx)\n",
    "out_loader = torch.utils.data.DataLoader(\n",
    "    out_set, batch_size=20, shuffle=False,\n",
    "    num_workers=0, pin_memory=True, drop_last=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### **MSP score calculation**\n",
    "This function demonstrates the calculation of MSP scores:\n",
    "\n",
    "$S_{MSP}(\\mathbf{x}; T)=S_{\\hat{y}}(\\mathbf{x}; T)=\\max _i \\frac{\\exp \\left(f_i(\\mathbf{x}) / T\\right)}{\\sum_{j=1}^N \\exp \\left(f_j(\\mathbf{x}) / T\\right)}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "MSP scores for ID dataset: [0.48711798 0.32653752 0.9923064  0.81736255 0.43308213 0.54241467\n",
      " 0.99980575 0.9986706  0.901668   0.26080698]\n",
      "MSP scores for OoD dataset: [0.9962953  0.7360485  0.59439    0.7490441  0.88971686 0.34063148\n",
      " 0.68121916 0.92814255 0.89619726 0.89417225]\n"
     ]
    }
   ],
   "source": [
    "def iterate_data_msp(data_loader, model, temper=1.0, n=None, seed=None):\n",
    "  confs = []\n",
    "  smax = torch.nn.Softmax(dim=-1).to(device)\n",
    "  if seed is not None:\n",
    "    setup_seed(seed)\n",
    "  for b, data in enumerate(data_loader):\n",
    "    x, y = data[0], data[1]\n",
    "\n",
    "    with torch.no_grad():\n",
    "      x = x.to(device)\n",
    "      logits = model(x)\n",
    "\n",
    "      conf, _ = torch.max(smax(logits / temper), dim=-1)\n",
    "      confs.extend(conf.data.cpu().numpy())\n",
    "      if n is not None and len(confs) >= n:\n",
    "        confs = confs[:n]\n",
    "        break\n",
    "\n",
    "  return np.array(confs)\n",
    "\n",
    "id_confs = iterate_data_msp(in_loader, model, n=10)\n",
    "print('MSP scores for ID dataset:', id_confs)\n",
    "ood_confs = iterate_data_msp(out_loader, model, n=10, seed=1)\n",
    "print('MSP scores for OoD dataset:', ood_confs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### **ODIN score calculation**\n",
    "This function demonstrates the calculation of ODIN scores by adding a small pertubation to the input $\\mathbf{x}$:\n",
    "\n",
    "$\\tilde{\\mathbf{x}}=\\mathbf{x}-\\varepsilon \\operatorname{sign}\\left(-\\nabla_{\\mathbf{x}} \\log S_{\\hat{y}}(\\mathbf{x} ; T)\\right)$\n",
    "\n",
    "$S_{ODIN}(\\tilde{\\mathbf{x}} ; T)=\\max _i \\frac{\\exp \\left(f_i(\\tilde{\\mathbf{x}}) / T\\right)}{\\sum_{j=1}^N \\exp \\left(f_j(\\tilde{\\mathbf{x}}) / T\\right)}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "torch.Size([20, 3, 32, 32])\n",
      "ODIN scores for ID dataset: [0.10058542 0.10024906 0.10060166 0.10059529 0.10050979 0.10061835\n",
      " 0.10113263 0.10131853 0.1003226  0.100205  ]\n",
      "torch.Size([20, 3, 32, 32])\n",
      "ODIN scores for OoD dataset: [0.10062252 0.10044716 0.10046107 0.10030256 0.10072093 0.10018507\n",
      " 0.10039657 0.10075594 0.10038517 0.10043416]\n"
     ]
    }
   ],
   "source": [
    "def iterate_data_odin(data_loader, model, epsilon=0.0, temper=1000, n=None, seed=None):\n",
    "  criterion = torch.nn.CrossEntropyLoss().to(device)\n",
    "  confs = []\n",
    "  if seed is not None:\n",
    "    setup_seed(seed)\n",
    "  for b, data in enumerate(data_loader):\n",
    "    with torch.enable_grad():\n",
    "      x, y = data[0], data[1]\n",
    "      x = Variable(x.to(device), requires_grad=True)\n",
    "      print(x.shape)\n",
    "\n",
    "      model.zero_grad()\n",
    "      outputs = model(x)\n",
    "\n",
    "      maxIndexTemp = np.argmax(outputs.data.cpu().numpy(), axis=1)\n",
    "      outputs = outputs / temper\n",
    "\n",
    "      labels = Variable(torch.LongTensor(maxIndexTemp).to(device))\n",
    "      loss = criterion(outputs, labels)\n",
    "      loss.backward()\n",
    "\n",
    "      # Normalizing the gradient to binary in {0, 1}\n",
    "      gradient = torch.ge(x.grad.data, 0)\n",
    "      gradient = (gradient.float() - 0.5) * 2\n",
    "      gradient[:,0] = (gradient[:,0] )/(63.0/255.0)\n",
    "      gradient[:,1] = (gradient[:,1] )/(62.1/255.0)\n",
    "      gradient[:,2] = (gradient[:,2] )/(66.7/255.0)\n",
    "\n",
    "      tempInputs = torch.add(x.data, gradient, alpha=-epsilon)\n",
    "      outputs = model(Variable(tempInputs))\n",
    "      outputs = outputs / temper\n",
    "      # Calculating the confidence after adding perturbations\n",
    "      nnOutputs = outputs.data.cpu().numpy()\n",
    "      nnOutputs = nnOutputs - np.max(nnOutputs, axis=1, keepdims=True)\n",
    "      nnOutputs = np.exp(nnOutputs) / np.sum(np.exp(nnOutputs), axis=1, keepdims=True)\n",
    "\n",
    "      confs.extend(np.max(nnOutputs, axis=1))\n",
    "      if n is not None and len(confs) >= n:\n",
    "        confs = confs[:n]\n",
    "        break\n",
    "\n",
    "  return np.array(confs)\n",
    "\n",
    "id_confs = iterate_data_odin(in_loader, model, epsilon=0.0014, temper=1000, n=10)\n",
    "print('ODIN scores for ID dataset:', id_confs)\n",
    "ood_confs = iterate_data_odin(out_loader, model, epsilon=0.0014, temper=1000, n=10, seed=1)\n",
    "print('ODIN scores for OoD dataset:', ood_confs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### **Energy score calculation**\n",
    "This function demonstrates the calculation of Energy scores for a discriminative neural classifier $f(\\mathbf{x}):\\mathbb{R}^D\\rightarrow\\mathbb{R}^K$:\n",
    "\n",
    "$E(\\mathbf{x}; f)=-T \\cdot \\log \\sum_i^K e^{f_i(\\mathbf{x}) / T}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Energy scores for ID dataset: [ 6.045364   3.0640268  5.5525904  5.84742    4.41186    6.2936645\n",
      "  9.92817   10.920949   4.1147766  3.0742755]\n",
      "Energy scores for OoD dataset: [8.589428  5.8669844 5.6057043 4.9404373 6.167072  3.0147164 5.6280694\n",
      " 6.3203053 6.2121897 6.0925484]\n"
     ]
    }
   ],
   "source": [
    "def iterate_data_energy(data_loader, model, temper=1.0, n=None, seed=None):\n",
    "  confs = []\n",
    "  if seed is not None:\n",
    "    setup_seed(seed)\n",
    "  for b, data in enumerate(data_loader):\n",
    "    with torch.no_grad():\n",
    "      x, y = data[0], data[1]\n",
    "      x = x.to(device)\n",
    "      # compute output, measure accuracy and record loss.\n",
    "      logits = model(x)\n",
    "\n",
    "      conf = temper * torch.logsumexp(logits / temper, dim=1)\n",
    "      confs.extend(conf.data.cpu().numpy())\n",
    "      if n is not None and len(confs) >= n:\n",
    "        confs = confs[:n]\n",
    "        break\n",
    "\n",
    "  return np.array(confs)\n",
    "\n",
    "id_confs = iterate_data_energy(in_loader, model, n=10)\n",
    "print('Energy scores for ID dataset:', id_confs)\n",
    "ood_confs = iterate_data_energy(out_loader, model, n=10, seed=1)\n",
    "print('Energy scores for OoD dataset:', ood_confs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### **GradNorm score calculation**\n",
    "This function demonstrates the calculation of GradNorm scores by calculating the gradient norm of the KL divergence:\n",
    "\n",
    "$D_{\\mathrm{KL}}(\\mathbf{u} \\| \\operatorname{softmax}(f(\\mathbf{x}))=-\\frac{1}{C} \\sum_{c=1}^C \\log \\frac{e^{f_c(\\mathbf{x}) / T}}{\\sum_{j=1}^C e^{f_j(\\mathbf{x}) / T}}-H(\\mathbf{u})$\n",
    "\n",
    "$S_{GradNorm}(\\mathbf{x})=\\left\\|\\frac{\\partial D_{\\mathrm{KL}}(\\mathbf{u} \\| \\operatorname{softmax}(f(\\mathbf{x}))}{\\partial \\mathbf{w}}\\right\\|_p$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "GradNorm scores for ID dataset: [6.285404  7.251757  7.534808  6.761553  7.2962017 5.492161  6.984316\n",
      " 7.9173303 6.3115664 7.591467 ]\n",
      "GradNorm scores for OoD dataset: [8.768083  7.059264  8.204973  4.9356623 6.09109   7.326145  6.725612\n",
      " 6.856453  7.900004  5.7272954]\n"
     ]
    }
   ],
   "source": [
    "def iterate_data_gradnorm(data_loader, model, model_name, temper=1.0, n=None, seed=None):\n",
    "  confs = []\n",
    "  logsoftmax = torch.nn.LogSoftmax(dim=-1).to(device)\n",
    "  if seed is not None:\n",
    "    setup_seed(seed)\n",
    "  for b, data in enumerate(data_loader):\n",
    "    with torch.enable_grad():\n",
    "      x, y = data[0], data[1]\n",
    "      inputs = Variable(x.to(device), requires_grad=True)\n",
    "\n",
    "      for data_i in inputs:\n",
    "        model.zero_grad()\n",
    "        output = model(data_i.unsqueeze(0))\n",
    "        targets = torch.ones((1, output.shape[-1])).to(device)\n",
    "        output = output / temper\n",
    "\n",
    "        loss = torch.mean(-targets * logsoftmax(output), dim=-1)\n",
    "        loss.backward()\n",
    "\n",
    "        if model_name == 'resnetv2':\n",
    "          layer_grad = model.head.conv.weight.grad.data\n",
    "        elif model_name == 'wrn':\n",
    "          layer_grad = model.fc.weight.grad.data\n",
    "\n",
    "        layer_grad_norm = torch.sum(torch.abs(layer_grad)).cpu().numpy()\n",
    "\n",
    "        confs.append(layer_grad_norm)\n",
    "        if n is not None and len(confs) >= n:\n",
    "          confs = confs[:n]\n",
    "          return np.array(confs)\n",
    "\n",
    "  return np.array(confs)\n",
    "\n",
    "id_confs = iterate_data_gradnorm(in_loader, model, 'wrn', n=10)\n",
    "print('GradNorm scores for ID dataset:', id_confs)\n",
    "ood_confs = iterate_data_gradnorm(out_loader, model, 'wrn', n=10, seed=1)\n",
    "print('GradNorm scores for OoD dataset:', ood_confs)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
