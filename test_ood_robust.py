import torch

from utils import log
from utils.test_utils import parse_args
from utils.dataloader import load_dataset_from_folder, load_dataset_from_torchvision
from utils.falsification import gen_noisy_data

from utils.eval import get_accuracy, test_prediction_verdicts, robust_eval
from models.model_utils import build_model
from utils.panel import display_distribution
import utils.monitor as monitor

def main(args):
    # Set device
    logger = log.setup_logger(args)
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    torch.backends.cudnn.benchmark = True

    # Load ID dataset
    test_set, test_loader = load_dataset_from_torchvision(args.data.id.folder, args.data.id.name, 
                                                          "test", args, shuffle=False)

    # Build model
    logger.info(f"Loading model from {args.model.path}")
    model = build_model(args, len(test_set.classes), device)

    # Generate noisy dataset from ID test dataset via random sampling
    if args.sampling.regen:
        if args.sampling.temp_distribution == 'id':
            datadir = args.data.id.folder
            dataset_name = args.data.id.name
        elif args.sampling.temp_distribution == 'ood':
            datadir = args.data.ood.folder
            dataset_name = args.sampling.temp_name

        # load dataset
        if dataset_name in ["LSUN-C", "LSUN-R", "iSUN", "Imagenet-C", "Imagenet-R"]:
            test_set_raw, test_loader_raw = load_dataset_from_folder(datadir, dataset_name, 
                                                                     args, split=None, shuffle=True, 
                                                                     raw=True)
        else:
            test_set_raw, test_loader_raw = load_dataset_from_torchvision(datadir, dataset_name, "test", 
                                                                         args, shuffle=True, raw=True, 
                                                                         download=False)
        gen_data, gen_labels, temp_data, idx_origin = gen_noisy_data(test_loader_raw, logger, args)
        return 0

    # Load template & noisy dataset
    temp_set, temp_loader = load_dataset_from_folder(args.sampling.save_path, 
                                                     args.sampling.temp_name.replace('-', '_')+'_temp', 
                                                     args, shuffle=False)
    noisy_set, noisy_loader = load_dataset_from_folder(args.sampling.save_path, 
                                                       args.sampling.temp_name.replace('-', '_')+'_gen_amp_' + str(args.sampling.method.a), 
                                                       args, shuffle=False)
    
    
    # display data distribution in the input space and feature space
    # display_distribution(temp_data.numpy(), gen_data.numpy(), idx_origin, 'input', logger, args)

    # y_temp, y_temp_pred, temp_moni_features = monitor.prediction_and_features(model, temp_loader, 
    #                                                                           device, moni_layers=[args.monitor.layer], 
    #                                                                           n=None, seed=None)
    # y_gen, y_gen_pred, gen_moni_features = monitor.prediction_and_features(model, noisy_loader, 
    #                                                                        device, moni_layers=[args.monitor.layer], 
    #                                                                        n=None, seed=None)
    # display_distribution(temp_moni_features[args.monitor.layer], gen_moni_features[args.monitor.layer], 
    #                      idx_origin, args.monitor.layer, logger, args)

    # Test model's prediction and OoD monitor verdicts on ID template dataset / noisy dataset
    # and record in file (OoD detector threshold@TPR=95)
    temp_set_name = args.sampling.temp_name.replace('-', '_')
    test_prediction_verdicts(model, temp_set, temp_loader, temp_set_name+"_temp", device, args)
    test_prediction_verdicts(model, noisy_set, noisy_loader, temp_set_name+"_gen_amp_"+str(args.sampling.method.a), device, args)

    # Show statistics of robust test results
    robust_eval(temp_set_name, logger, args)


if __name__ == "__main__":

    args = parse_args(configs='configs/test_ood_robust_config.yaml')
    main(args)
