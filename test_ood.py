import torch
import time

from utils import log
from utils.test_utils import parse_args
from utils.eval import get_accuracy, run_eval
from utils.dataloader import make_id_ood
from models.model_utils import build_model

def main(args):
    # Set device
    logger = log.setup_logger(args)
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    torch.backends.cudnn.benchmark = True

    # Load ID and OoD datasets
    in_set, out_set_list, in_loader, out_loader_list = make_id_ood(args, logger)

    # Build model
    logger.info(f"Loading model from {args.model.path}")
    model = build_model(args, len(in_set.classes), device)

    # ID accuracy
    get_accuracy(model, in_loader, logger, device)
    
    # OoD detection performance evaluation
    for i, out_loader in enumerate(out_loader_list):
        logger.info(f"============OoD dataset: {args.data.ood.name[i].upper()}============")
        run_eval(model, in_loader, out_loader, logger, args, 
                 len(in_set.classes), device)

if __name__ == "__main__":

    args = parse_args(configs='configs/test_ood_config.yaml')
    main(args)
