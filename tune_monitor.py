import torch
import time

from utils import log
from utils.test_utils import parse_args
from utils.dataloader import load_dataset_from_folder, load_dataset_from_torchvision, make_id_ood
from utils.monitor import sort_class_wise_features, prediction_and_features, tune_monitor
from models.model_utils import build_model
from utils.monitor import iterate_data_monitor

def main(args):

    # Set device
    logger = log.setup_logger(args)
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    torch.backends.cudnn.benchmark = True

    # Load training datasets
    train_dataset, train_dataloader = load_dataset_from_torchvision(args.data.id.folder, args.data.id.name, "train", 
                                                                    args, shuffle=False)

    # Build model
    logger.info(f"Loading model from {args.model.path}")
    model = build_model(args, len(train_dataset.classes), device)

    # Feature extraction of training dataset
    start_time = time.time()
    y_train, y_train_pred, y_train_moni_features = prediction_and_features(model, train_dataloader, device, 
                                                                           moni_layers=[args.monitor.layer])
    sort_class_wise_features(y_train, y_train_pred, y_train_moni_features, args.monitor.storage_folder)
    end_time = time.time()
    logger.info("Feature extraction time: {}".format(end_time - start_time))

    # Load ID and OoD datasets
    in_set, out_set_list, in_loader, out_loader_list = make_id_ood(args, logger)

    # # Monitor performance evaluation (tune monitor hyperparameter tau to obtain FPR95)
    # taus, tprs = tune_monitor(model, in_loader, logger, args,
    #                           len(in_set.classes), device, enriched=args.monitor.enrich)
    # logger.info(f"taus={str(taus)}, tprs={str(tprs)}")

    # taus = [236, 329, 170, 0.0001, 232, 0.0001, 208, 308, 194, 358]
    taus = [0.03139, 0.02383, 0.02844, 0.0365, 0.02844, 0.02844, 0.02383, 0.02383, 0.02844, 0.01692]

    from utils.monitor import get_monitor_tpr_fpr

    # y_pred = torch.empty(0)
    in_verdicts = iterate_data_monitor(in_loader, model, device, args.benchmark, args.monitor.layer, 
                                       args.monitor.storage_folder, range(len(in_set.classes)), 
                                       taus, pos=args.eval.pos, enriched=args.monitor.enrich)
    y_pred = torch.from_numpy(in_verdicts)
    y_true = torch.ones_like(y_pred)
    for i, out_loader in enumerate(out_loader_list):
        logger.info(f"================= {args.data.ood.name[i].upper()} ==================")
        out_verdicts = iterate_data_monitor(out_loader, model, device, args.benchmark, args.monitor.layer, 
                                            args.monitor.storage_folder, range(len(in_set.classes)), 
                                            taus, pos=args.eval.pos, n=args.eval.num_ood, seed=args.seed, 
                                            enriched=args.monitor.enrich)
        out_verdicts = torch.from_numpy(out_verdicts)
        y_pred = torch.concat((y_pred, out_verdicts))
        y_true = torch.concat((y_true, torch.zeros_like(out_verdicts)))
        tpr, fpr = get_monitor_tpr_fpr(y_true, y_pred)

        logger.info(f"tpr: {tpr}")
        logger.info(f"fpr: {fpr}")
    
if __name__ == "__main__":

    args = parse_args(configs='configs/tune_monitor_config.yaml')
    main(args)
