# Benchmarking OoD Detection Methods

Code is modified from [GradNorm](https://github.com/deeplearning-wisc/gradnorm_ood) offical implementation source code.

## Usage

### 1. Dataset Preparation

#### In-distribution dataset

Please download [CIFAR-10](https://cloud.univ-grenoble-alpes.fr/s/ipgfAwg4Fk4CyPd) and place the ID dataset in `./dataset/id_data/`.

#### Out-of-distribution dataset

Please download [Textures](https://cloud.univ-grenoble-alpes.fr/s/RHZcTbPJqjwQkkk), [SVHN](https://cloud.univ-grenoble-alpes.fr/s/oaRAzmedmCxxSgf), [LSUN-C](https://cloud.univ-grenoble-alpes.fr/s/cDnrfzr3zF288xk), [LSUN-R](https://cloud.univ-grenoble-alpes.fr/s/Pa8YEZCJRNKtaCe), [iSUN](https://cloud.univ-grenoble-alpes.fr/s/YfAkELSf6PfiaN2), [Places365](https://cloud.univ-grenoble-alpes.fr/s/JDHFcn2Z44FYERp), and put all downloaded OOD datasets into `./dataset/ood_data/`.

### 2. Model Preparation

We omit the process of pre-training a classification model on the benchmark.
For the ease of reproduction, we provide our trained models [cifar10_wrn_normal_standard_epoch_199.pt](https://cloud.univ-grenoble-alpes.fr/s/xbZ4R65j9KGnqiN), [cifar10_wrn_logitnorm_standard_epoch_199.pt](https://cloud.univ-grenoble-alpes.fr/s/CpqQK3YrsQ23xqD)

Please put the downloaded model in `./checkpoints/`.

### 4. Monitor Construction
To build monitors from the training dataset, please run:
```
python build_monitors.py [--moni_layers minus_2 minus_1] [--taus 1.0 0.1 0.01]
```
The parameters can be defined in file ./configs/build_monitors.yaml

### 5. OOD Detection Evaluation

To evaluate various OoD detection methods and reproduce our test results, please run:
```
python test_ood.py [--use_monitor] [--moni_layer minus_2] [--tau 0.01]
```
The parameters can be defined in file ./configs/test_ood.yaml
