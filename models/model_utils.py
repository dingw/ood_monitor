import torch

import models.resnetv2 as resnetv2
import models.wrn as wrn

def build_model(args, n_classes, device):
    state_dict = torch.load(args.model.path, map_location=device)
    if args.model.name == 'resnetv2':
        model = resnetv2.KNOWN_MODELS[args.model](head_size=n_classes)
        model.load_state_dict_custom(state_dict['model'])
    elif args.model.name == 'wrn':
        model = wrn.KNOWN_MODELS[args.model.name.upper()+'-'+str(args.model.depth)+'-'+str(args.model.widden_factor)](head_size=n_classes, dropRate=args.model.droprate)
        model.load_state_dict(state_dict)
    model = model.to(device)

    return model