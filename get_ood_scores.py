import torch
import logging

from utils.test_utils import HierarchicalDict
from utils.dataloader import load_dataset_from_folder
from models.model_utils import build_model
from utils.eval import get_ood_scores

from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
import numpy as np
import pickle

"""
Setups

"""
benchmark = "CIFAR10"
ood_names = ["Textures", "SVHN", "LSUN-C", "LSUN-R", "iSUN", "Places365"]
args = {'benchmark': benchmark, 'preprocess': {'mean': [0.492, 0.482, 0.446], 'std': [0.247, 0.244, 0.262], 
                                               'input_size': 32, 'batch_size': 20, 'workers': 8}, 
        'model': {'name': 'wrn', 'depth': 40, 'widden_factor': 2, 'droprate': 0.3, 
                  'path': 'checkpoints/'+benchmark.lower()+'_wrn_normal_standard_epoch_199.pt'}, 
        'data': {'id': {'name': benchmark, 'folder': 'dataset/id_data/'}, 
                 'ood': {'name': ood_names, 'folder': 'dataset/ood_data/'}}, 
        'scores': {'msp': {'T': 1.0}, 'odin': {'T': 1000, 'epsilon': 0.0014}, 'energy': {'T': 1.0}, 'gradnorm': {'T': 1.0}},
        'eval': {'pos': 'id', 'num_ood': 2000, 'num_to_avg': 5}, 'seed': 1, 'logdir': 'checkpoints/test_log'}

args = HierarchicalDict(args)

device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
print("Device:", device)
logger = logging.getLogger()

amplitudes = [1, 2, 4, 6, 8, 10]

"""
Step 1: Get CNN outputs of ID/OoD datasets 

"""

# Load model
model = build_model(args, 10, device)
model.eval()

# Load ID/OoD datasets
for amp in amplitudes:

    save_path = 'dataset/id_cifar10_noisy_data/'
    in_temp_set, in_temp_loader = load_dataset_from_folder(save_path, 'CIFAR10_temp', args, shuffle=False)
    in_noisy_set, in_noisy_loader = load_dataset_from_folder(save_path, 'CIFAR10_gen_amp_'+str(amp)+'/', args, shuffle=False)

    out_temp_loader_list = []
    out_noisy_loader_list = []
    for ood_name in ood_names:
        
        save_path = 'dataset/ood_' + ood_name.lower() + '_noisy_data/'
        out_temp_set, out_temp_loader = load_dataset_from_folder(save_path, ood_name+'_temp', args, shuffle=False)
        out_noisy_set, out_noisy_loader = load_dataset_from_folder(save_path, ood_name+'_gen_amp_'+str(amp)+'/', 
                                                                    args, shuffle=False)
        out_temp_loader_list.append(out_temp_loader)
        out_noisy_loader_list.append(out_noisy_loader)



    # Collect CNN outputs
    print("Collecting CNN outputs.")
    in_temp_outputs = torch.empty(0, dtype=torch.float32).to(device)
    in_temp_true_labels = torch.empty(0, dtype=torch.int8)
    in_noisy_outputs = torch.empty(0, dtype=torch.float32).to(device)
    in_noisy_true_labels = torch.empty(0, dtype=torch.int8)
    out_temp_outputs_dict = dict()
    out_noisy_outputs_dict = dict()

    with torch.no_grad():
        for data in in_temp_loader:
            X = data[0].to(device); y = data[1]
            out = model(X)
            in_temp_outputs = torch.concat((in_temp_outputs, out), axis=0)
            in_temp_true_labels = torch.concat((in_temp_true_labels, y))

        for data in in_noisy_loader:
            X = data[0].to(device); y = data[1]
            out = model(X)
            in_noisy_outputs = torch.concat((in_noisy_outputs, out), axis=0)
            in_noisy_true_labels = torch.concat((in_noisy_true_labels, y))

        for i, out_temp_loader in enumerate(out_temp_loader_list): 
            out_temp_outputs = torch.empty(0, dtype=torch.float32).to(device)
            out_noisy_outputs = torch.empty(0, dtype=torch.float32).to(device)
            for data in out_temp_loader:
                X = data[0].to(device)
                out = model(X)
                out_temp_outputs = torch.concat((out_temp_outputs, out), axis=0)
            out_temp_outputs_dict[ood_names[i]] = out_temp_outputs.cpu()
            
            for data in out_noisy_loader:
                X = data[0].to(device)
                out = model(X)
                out_noisy_outputs = torch.concat((out_noisy_outputs, out), axis=0)
            out_noisy_outputs_dict[ood_names[i]] = out_noisy_outputs.cpu()

    in_temp_outputs = in_temp_outputs.cpu()
    in_noisy_outputs = in_noisy_outputs.cpu()

    """
    Step 2: Get OoD scores of ID/OoD datasets

    """

    print("Calculating OoD scores.")

    in_temp_scores = get_ood_scores(model, in_temp_loader, device, args)
    in_noisy_scores = get_ood_scores(model, in_noisy_loader, device, args)

    out_temp_scores_dict = dict()
    out_noisy_scores_dict = dict()
    for i, out_temp_loader in enumerate(out_temp_loader_list):
        out_temp_scores = get_ood_scores(model, out_temp_loader, device, args)
        out_temp_scores_dict[ood_names[i]] = out_temp_scores
    for i, out_noisy_loader in enumerate(out_noisy_loader_list):
        out_noisy_scores = get_ood_scores(model, out_noisy_loader, device, args)
        out_noisy_scores_dict[ood_names[i]] = out_noisy_scores


    # Save to OoD scores to file
    

    folder_path = './checkpoints/intermediate_results/CIFAR10/ood_scores/'

    # OoD scores
    file_name = 'in_temp_ood_scores.pkl'
    with open(folder_path+file_name, 'wb') as f:
        pickle.dump(in_temp_scores, f)
    file_name = 'in_noisy_amp_'+str(amp)+'_ood_scores.pkl'
    with open(folder_path+file_name, 'wb') as f:
        pickle.dump(in_noisy_scores, f)

    file_name = 'out_temp_ood_scores.pkl'
    with open(folder_path+file_name, 'wb') as f:
        pickle.dump(out_temp_scores_dict, f)
    file_name = 'out_noisy_amp_'+str(amp)+'_ood_scores.pkl'
    with open(folder_path+file_name, 'wb') as f:
        pickle.dump(out_noisy_scores_dict, f)

    # True labels
    file_name = 'in_temp_true_labels.pkl'
    with open(folder_path+file_name, 'wb') as f:
        pickle.dump(in_temp_true_labels, f)
    file_name = 'in_noisy_amp_'+str(amp)+'_true_labels.pkl'
    with open(folder_path+file_name, 'wb') as f:
        pickle.dump(in_noisy_true_labels, f)

    file_name = 'in_temp_outputs.pkl'
    with open(folder_path+file_name, 'wb') as f:
        pickle.dump(in_temp_outputs, f)
    file_name = 'in_noisy_amp_'+str(amp)+'_outputs.pkl'
    with open(folder_path+file_name, 'wb') as f:
        pickle.dump(in_noisy_outputs, f)

    # Model outputs
    file_name = 'out_temp_outputs.pkl'
    with open(folder_path+file_name, 'wb') as f:
        pickle.dump(out_temp_outputs_dict, f)
    file_name = 'out_noisy_amp_'+str(amp)+'_outputs.pkl'
    with open(folder_path+file_name, 'wb') as f:
        pickle.dump(out_noisy_outputs_dict, f)