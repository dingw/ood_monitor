import numpy as np
from sklearn.neighbors import KernelDensity
import torch
from torch.distributions import Distribution
import itertools


# This class use KDE to approximate a multivariate distribution from a dataset with n features
class CustomMultivariateKDE(Distribution):
    arg_constraints = {}
    def __init__(self, data, bandwidth=0.5):

        self.data = data
        self.bandwidth = bandwidth
        self.kde_models = []

        for dim in range(data.shape[1]):
            kde_model = KernelDensity(bandwidth=self.bandwidth)
            kde_model.fit(data[:, dim].reshape(-1, 1))
            self.kde_models.append(kde_model)

        batch_shape = torch.Size([1])
        event_shape = torch.Size([data.shape[1]])
        super(CustomMultivariateKDE, self).__init__(batch_shape, event_shape)

    def sample(self, sample_shape=torch.Size()):
        num_samples = sample_shape.numel()
        samples = []
        for dim in range(self.event_shape[0]):
            dim_samples = self.kde_models[dim].sample(num_samples)
            samples.append(dim_samples)

        samples = np.column_stack(samples)
        return torch.tensor(samples)

    def log_prob(self, value):
        log_probs = []
        for dim in range(self.event_shape[0]):
            log_prob_dim = self.kde_models[dim].score_samples(value[:, dim].reshape(-1, 1))
            log_probs.append(log_prob_dim)

        log_probs = np.column_stack(log_probs)
        return torch.tensor(log_probs)

# Uniform distribution
class CustomMultivariateUniform(Distribution):
    arg_constraints = {}
    def __init__(self, low, high):
        """
        Initialize a custom multivariate uniform distribution within the specified range [low, high]
        for all dimensions.

        :param low: Lower bound of the distribution (tensor of low bounds for each dimension)
        :param high: Upper bound of the distribution (tensor of high bounds for each dimension)
        """
        self.low = low
        self.high = high
        super(CustomMultivariateUniform, self).__init__(batch_shape=torch.Size())

    def sample(self, sample_shape=torch.Size()):
        """
        Generate samples from the custom multivariate uniform distribution.

        :param sample_shape: Shape of the samples to generate
        :return: Sampled values
        """
        # Generate random samples within the specified range for each dimension
        samples = torch.rand(sample_shape + self.low.shape) * (self.high - self.low) + self.low
        return samples

    def log_prob(self, value):
        """
        Compute the log probability density for the given value.

        :param value: Values to compute the log probability for
        :return: Log probability density
        """
        # Check if values are within the specified range for each dimension
        mask = (value >= self.low) & (value <= self.high)
        # Sum the log probabilities for each dimension
        log_prob = torch.where(mask.all(dim=-1, keepdim=True), -torch.log(self.high - self.low), torch.full_like(value, -float('inf')))
        return log_prob

# This function calculated the KL divergence between 2 multivariate distributions
def cal_kl_div(p, q, dimensions):
    # discretization
    num_points = 101
    interval = 1 / (num_points - 1)
    x_values = np.linspace(0, 1, num_points, dtype=float)
    points = list(itertools.product(x_values, repeat=dimensions))

    # initialize KL divergence
    kl_divergences = []
    p_log_probs = p.log_prob(torch.tensor(points))
    q_log_probs = q.log_prob(torch.tensor(points))

    for dim in range(dimensions):
        kl_divergence = 0
        for pt in range(len(points)):
            p_log_prob = p_log_probs[pt, dim].item()
            q_log_prob = q_log_probs[pt, dim].item()
            if np.exp(p_log_prob) > 0 and np.exp(q_log_prob) > 0 :
                kl_divergence += np.exp(p_log_prob) * (p_log_prob - q_log_prob) * interval * interval
        kl_divergences.append(kl_divergence)
    print("KL Divergence:", kl_divergences)
    
    return kl_divergences
